#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "so_game_protocol.h"


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


int main(){
	int sockets = socket(PF_INET, SOCK_STREAM, 0);
	if(socket<0){
		printf("Errore creazione socket\n" );
		return 0;
	}

	struct sockaddr_in happy;
	happy.sin_family = AF_INET;
	happy.sin_addr.s_addr = inet_addr("127.0.0.1");
	happy.sin_port = htons(27015);

	int ret;
	ret = bind(sockets, (struct sockaddr*) &happy, sizeof(happy));
	if (ret<0){
		printf("Errore binding socket\n" );
		return 0;
	}

	ret = listen(sockets,1);
	if (ret<0){
		printf("Errore listen socket\n" );
		return 0;
	}

	int socket_client;
	struct sockaddr_in sohappy;

	PacketHeader* pack;

	while(1){
		int dim = sizeof(sohappy);
		socket_client = accept(sockets, (struct sockaddr*) &sohappy, &dim);
		if (socket_client < 0){
			printf("Errore stabilimento connessione con client\n" );
			return 0;
		}

		char buf[1000000];
		ret = recv(socket_client, buf, 1000000, 0);
		if(ret<=0){
			printf("Errore nella ricezione \n" );
		}
		int* tmp = (int*)malloc(sizeof(int));

		memcpy(tmp, buf, 4);
		printf("La dimensione del pacchetto serializzato e' %d\n", *tmp );

		pack = Packet_deserialize(buf+4,*tmp);
		printf("[SERVER]MIAO\n");

		switch (pack->type){
			case 0x1:
						printf("[SERVER] Ho ricevuto un pacchetto di tipo ID\n");
						IdPacket* testid = (IdPacket*)pack;
						printf("[SERVER] L'id del packetto e' %d\n",testid->id );
						break;
			case 0x4:
						printf("[SERVER] Ho ricevuto un pacchetto di tipo IMG\n");
						ImagePacket* testimg = (ImagePacket*)pack;
						printf("[SERVER] L'id del packetto e' %d\n",testimg->id );
						printf("L' immagine e' grande %d \n",testimg->header.size );
						Image_save(testimg->image,"output.pgm");
						break;	

			default:
						printf("[SERVER] Errore nel case switch\n");
						
		} 

		//IdPacket* test = (IdPacket*)pack;
		//printf("[SERVER] L'id del packetto e' %d\n",test->id );

	}
	return 1;
}



