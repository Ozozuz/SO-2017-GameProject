#include "utils.h"
#include <stdio.h>
#include <string.h>
#include "common.h"

#define clear() system("clear");

void* autenticationHandler(dataForAutentication* data){
	char answer[128];
	int todo=1;

	sleep(1);

	while(todo){
		printf("Welcome to SO-GAME-Project\n");
		printf("Have you already played the game? y/n: ");
		scanf("%s",answer);
		if(strlen(answer)>1){
			clear();
		continue;
		}
		if( strcmp(answer,"y") == 0){
			loginHandler(data);
		}
		else if ( strcmp(answer,"n")==0) {
			registrationHandler(data);
		}
		else{
			clear();
			continue;
		}
		todo=0;
	}
	return NULL;
}

void* loginHandler(dataForAutentication* data){
	clear();
	char usr[128];
	char psw[128];
	char check[128];

	int maxtry=3, ret;

	data->id = newIdPacket();
	data->id->id = -1;
	sendTCPPacket((const PacketHeader*) data->id, data->socket);
	while(maxtry){
		clear();
		printf("You have %d attempts to log-in\n",maxtry);
		printf("Insert your username: ");
		scanf("%s",usr);
		printf("Your password: ");
		scanf("%s",psw);

		ret=send(data->socket, usr, 128, 0);
        ERROR_HELPER(ret, "Error sending usr");

		ret=send(data->socket, psw, 128, 0);
        ERROR_HELPER(ret, "Error sending psw");

		ret=recv(data->socket, check, 128, MSG_WAITALL);
        ERROR_HELPER(ret, "Error receiving check");

		if(strcmp(check,"usr")==0){
			clear();
			printf("Username not found !\n");
			sleep(2);
			clear();
			maxtry--;
		}
		else if(strcmp(check,"ingame")==0){
			clear();
			printf("You are already logged-in, security issue maybe?\n");
			sleep(2);
			clear();
			maxtry--;
		}
		else if(strcmp(check,"psw")==0){
			clear();
			printf("Wrong password for the provided Username\n");
			sleep(2);
			clear();
			maxtry--;

		}
		else if(strcmp(check,"ok")==0){
			printf("Login Approved\n");
		    data->id 		 = (IdPacket*) receiveAndDeserializeTCP(data->socket);
			int id 			 = data->id->id;
			data->mapTexture = newImagePacket(id, NULL, GetTexture);
			data->Elevation  = newImagePacket(id, NULL, GetElevation);


			sendTCPPacket((const PacketHeader*)data->mapTexture, data->socket);
			sendTCPPacket((const PacketHeader*)data->Elevation, data->socket);

		  	data->mapTexture 		= (ImagePacket*) receiveAndDeserializeTCP(data->socket);
		  	data->Elevation  		= (ImagePacket*) receiveAndDeserializeTCP(data->socket);
		  	data->Image 			= (ImagePacket*) receiveAndDeserializeTCP(data->socket);
		  	data->position          = (PositionPacket*) receiveAndDeserializeTCP(data->socket);
		  	data->autenticated      = 1;
		  	printf("Login Completed\n");
		  	sleep(2);
		  	clear();
		  	return NULL;
		}
	}
	return NULL;
}

void* registrationHandler(dataForAutentication* data){
	clear();
	char usr[128];
	char psw[128];
    char msg[128];
    char pathname[128];
	int needimage=1;
	int needpsw=1;
	int choosen;
	int id;
	int ret, ok;

	data->id = newIdPacket();
    data->id->id=0; //metto id=0 per far sapere al server che mi sto registrando
    data->position=NULL;
	sendTCPPacket((const PacketHeader*) data->id, data->socket);

    do{
        printf("Choose your username 'max 8 char': ");
		scanf("%s",usr);
		if(strlen(usr)>8){
			clear();
			printf("Choose an username with max 8 char!!\n");
			sleep(2);
			clear();
			continue;
		}

        ret=send(data->socket, usr, 128, 0);
        ERROR_HELPER(ret, "Error sending usr");

        ret=recv(data->socket, msg, 128, 0);
        ERROR_HELPER(ret, "Error receiving msg");

        if (strcmp(msg, "OK")==0) ok=1;
        else if (strcmp(msg, "KO")==0){
            ok=0;
            sleep(1);
            clear();
            printf("Username already exists! Insert anotherone..\n");
            sleep(1);
        }

    } while(!ok);

    while(needpsw){
		//fflush(stdin);
		printf("Now choose your psw 'max 8 char: ");
		scanf("%s",psw);
		if(strlen(psw)>8){
			printf("\nChoose a password with max 8 char!!\n");
			sleep(2);
			clear();
			continue;
		}
		needpsw=0;
	}

	while(needimage){
		clear();
		printf("Now you have to choose your texture, just type the numer\n1-RED\n2-BLUE\n3-GREEN\n4-YELLOW\n5-DEFAULT\n6-CUSTOM\n");
		printf("Input number: ");
		scanf("%d",&choosen);
		if(!choosen>=1 && !choosen<=6 || !choosen==401){
			clear();
			continue;
		}

		clear();
		switch(choosen){
			case 1:			data->text_for_serv = Image_load("images/red.ppm");
							break;
			case 2:			data->text_for_serv = Image_load("images/blue.ppm");
							break;
			case 3:			data->text_for_serv = Image_load("images/green.ppm");
							break;
			case 4:			data->text_for_serv = Image_load("images/yellow.ppm");
							break;
			case 5:			data->text_for_serv = NULL;
							break;
			case 6:			printf("To add a custom skin you have to provide it's path: ");
							scanf("%s",pathname);
							data->text_for_serv = Image_load(pathname);
							if(!data->text_for_serv){
								printf("Error has occurred, loading default texture\n");
								data->text_for_serv = NULL;
							}
							break;
			case 401:		data->text_for_serv = Image_load("images/tmp/gris.ppm");
							break;
			default:		break;

		}
		needimage=0;
	}



	ret=send(data->socket, psw, 128, 0);
    ERROR_HELPER(ret, "Error sending psw");

    data->id 		 = (IdPacket*) receiveAndDeserializeTCP(data->socket);
	id 			     = data->id->id;
	data->mapTexture = newImagePacket(id, NULL, GetTexture);
	data->Elevation  = newImagePacket(id, NULL, GetElevation);

	if (data->text_for_serv==NULL){
    	printf("[CLIENT] Server will provide you default texture\n" );
	  	data->Image = newImagePacket(id, NULL, GetTexture);
  	}
  	else
  		data->Image = newImagePacket(id, data->text_for_serv, PostTexture);

	sendTCPPacket((const PacketHeader*)data->mapTexture, data->socket);
	sendTCPPacket((const PacketHeader*)data->Elevation, data->socket);
	sendTCPPacket((const PacketHeader*)data->Image, data->socket);

  	data->mapTexture 		= (ImagePacket*) receiveAndDeserializeTCP(data->socket);
  	data->Elevation  		= (ImagePacket*) receiveAndDeserializeTCP(data->socket);
  	data->Image 			= (ImagePacket*) receiveAndDeserializeTCP(data->socket);
  	data->autenticated 		= 1;
  	return NULL;
}


dataForAutentication* newDataForAutentication(){
	dataForAutentication* new = (dataForAutentication*)malloc(sizeof(dataForAutentication));
	new->text_for_serv        = NULL;
	new->autenticated         = 0;
	return new;
}

int valid_username(linked_list* clients, char* usr){
    linked_list_node* aux=clients->head;
    while(aux!=NULL){
        if (strcmp(aux->usr, usr)==0) return 0;
        aux=aux->next;
    }
    return 1;
}


IdPacket* newIdPacket(){
	PacketHeader head;
	IdPacket* packet = (IdPacket*)malloc(sizeof(IdPacket));

	head.type = GetId;
	packet->header = head;
	packet->id = -1;

	return packet;
}

ImagePacket* newImagePacket(int id, Image* img, Type mode){
	PacketHeader head;
	ImagePacket* packet = (ImagePacket*)malloc(sizeof(ImagePacket));

	head.type = mode;
	packet->id = id;
	packet->image = img;
	packet->header  = head;

	return packet;
}

VehicleUpdatePacket* newVehicleUpdatePacket(int id, float r_f, float t_f){

	PacketHeader head;
	VehicleUpdatePacket* packet = (VehicleUpdatePacket*)malloc(sizeof(VehicleUpdatePacket));

	head.type = VehicleUpdate;
	packet->header = head;
	packet->id     = id;
	packet->rotational_force = r_f;
	packet->translational_force = t_f;

	return packet;
}

WorldUpdatePacket* newWUP(World* w){
	PacketHeader h;
	h.type = WorldUpdate;

	ClientUpdate* c =(ClientUpdate*) malloc(sizeof(ClientUpdate)*(w->vehicles.size));

	ListItem* item=w->vehicles.first;
	int i=0;

  	while(item){
    	Vehicle* v=(Vehicle*)item;
    	c[i].id=v->id;
    	c[i].x =v->x;
    	c[i].y =v->y;
    	c[i].theta =v->theta;
    	item=item->next;
    	i++;
 	 }

 	 WorldUpdatePacket* p = (WorldUpdatePacket*)malloc(sizeof(WorldUpdatePacket));
 	 p->header = h;
 	 p->num_vehicles = w->vehicles.size;
 	 p->updates = c;

 	 return p;
}

int newTCPConnection(char* ip, int port, struct sockaddr_in* str ){
	int sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(sock<0){
			printf("[CLIENT]Errore creazione socket\n");
			return 0;
	}
	int ret;
	int dim = sizeof(*str);
	str->sin_family = AF_INET;
	str->sin_addr.s_addr = inet_addr(ip);
	str->sin_port = htons(port);
	ret = connect(sock, (struct sockaddr*) str, dim);
	if(ret<0){
		printf("Errore connessione lato client");
	}
	return sock;
}

void sendTCPPacket(const PacketHeader* packet, int sock){
	char buf[1000000];
	int* intposition = (int*)(buf);
	int ret;
	int size;
	size = Packet_serialize(buf+sizeof(int), packet);
	*intposition = size;
	ret = send(sock, buf, 1000000, 0);
	if(ret != 1000000){
		printf("Errore nell' invio \n");
		return ;
	}
	return;
}

void sendUDPPacket(const PacketHeader* packet, int sock, struct sockaddr_in* happy){

	//TODO: 1mb è troppo
	char buf[1024];
	int dim = sizeof(struct sockaddr_in);

	int* intposition = (int*)(buf);
	int size;

	size = Packet_serialize(buf+sizeof(int), packet);
	*intposition = size;

	sendto(sock, buf, 1024, 0,(struct sockaddr*)happy, dim );
}

PacketHeader* receiveAndDeserializeTCP(int sock){
	char buf[1000000];
	int ret;
	PacketHeader* ris;
	int* packetsize = (int*)malloc(sizeof(int));

	ret = recv(sock, buf, 1000000, MSG_WAITALL);
	if(ret<=0){
		printf("Errore nella ricezione \n" );
        return NULL;
	}
	memcpy(packetsize, buf, 4);
	ris = Packet_deserialize(buf+4,*packetsize);

	return ris;
}

PacketHeader* receiveAndDeserializeUDP(int sock, struct sockaddr_in* happy){
	char buf[1024];
	int ret;
	int dim = sizeof(struct sockaddr_in);

	PacketHeader* ris;
	int* packetsize = (int*)malloc(sizeof(int));
	ret = recvfrom(sock, buf, 1024, 0, (struct sockaddr*) happy, (socklen_t*) &dim);
	if(ret<=0){
		printf("Errore nella ricezione \n" );
        return NULL;
	}
	memcpy(packetsize, buf, 4);
	ris = Packet_deserialize(buf+4,*packetsize);

	return ris;
}

void updatePosition(const PacketHeader* packet, World* wrld, int id){
	int i;
	WorldUpdatePacket* pack = ( WorldUpdatePacket* )packet;
	int servernumveic = pack->num_vehicles;
	for(i=0; i<servernumveic; i++){
		if(1){
			ClientUpdate pos = pack->updates[i];
			Vehicle* tmp     = World_getVehicle(wrld, pos.id);
			tmp->x = pos.x;
			tmp->y = pos.y;
			tmp->theta = pos.theta;
		}
	}
}

void addNewPlayer(const PacketHeader* packet, struct sockaddr_in* happy, World* wrld, int id, int sockTCP, int sockUDP){
	WorldUpdatePacket* pack = ( WorldUpdatePacket* )packet;
	int servernumveic = pack->num_vehicles;
	int mynumveic     = wrld->vehicles.size;
	int i;

	if(mynumveic<servernumveic){
		printf("[CLIENT]E' arrivato un nuovo giocatore !\n");
		for(i=0; i<servernumveic; i++){
			ClientUpdate tmp = pack->updates[i];
			if( (World_getVehicle( wrld, tmp.id) == 0) ){
				printf("Sono id=%d e sto aggiungendo id=%d\n", id, tmp.id);
				IdPacket* reqInfo    = newIdPacket();
				reqInfo->id          = id;
				reqInfo->header.type = GetTexture;
				ImagePacket* req     = newImagePacket(tmp.id, NULL, GetTexture);

				sendUDPPacket( (const PacketHeader*)reqInfo, sockUDP, happy);
				sendTCPPacket( (const PacketHeader*)req, sockTCP );
				ImagePacket* answer  = (ImagePacket*)receiveAndDeserializeTCP(sockTCP);

				Vehicle* new_vehicle=(Vehicle*) malloc(sizeof(Vehicle));
				char dir[64];

				//sprintf(dir,"Im%dfromp%d.ppm",id,tmp.id);
				//Image_save(answer->image, dir);
				//Image* try    = Image_load(dir);
				Vehicle_init(new_vehicle, wrld, answer->id ,answer->image );
				World_addVehicle(wrld, new_vehicle);

				/*
				char dir[32];
				sprintf(dir,"p%dfromp%d.ppm",id,tmp.id);
				Image_save(answer->image, dir);
				Image* text = Image_load(dir);
				Vehicle_init(new_vehicle, wrld, answer->id ,text );
				Vehicle_applyTexture(new_vehicle, text);
				World_addVehicle(wrld, new_vehicle);
				Vehicle_applyTexture(new_vehicle, text);
				*/



			}
		}
	}

	else if (mynumveic>servernumveic) {
		printf("Si e' disconnesso un giocatore\n");
		ListItem* item=wrld->vehicles.first;
		int find = 0;

 		while(item){

    		Vehicle* v=(Vehicle*)item;

    		for(i=0; i<servernumveic; i++){

    			if(pack->updates[i].id == v->id)
    				find = 1;

    		}

    		if (find == 0)
    			World_detachVehicle(wrld, v);


      		find = 0;
    		item=item->next;
  		}

	}
}

int assign_ID(int* connected, int max_num) {
	int id;

	for (int i=1; i<max_num;i++){

		if (connected[i]==0){
			id = i;
			break;
		}


	}


	return id;
}
