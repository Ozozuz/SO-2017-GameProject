#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(){

	int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sock<0){
		printf("error creating socket\n");
		return 0;
	}

	struct sockaddr_in client_addr;

	client_addr.sin_family = AF_INET;
	client_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	client_addr.sin_port = htons(27014);

	char msg[1024];
	int dim = sizeof(client_addr);

	while(1){

		printf("msg: ");
		scanf("%s",msg);
		int ret = sendto(sock, msg, strlen(msg), 0, (struct sockaddr *) &client_addr, dim );
		if ( ret<0 ){
			printf("Errore invio msg\n" );
			return 0;
			
		}
	}

	return 0;
}