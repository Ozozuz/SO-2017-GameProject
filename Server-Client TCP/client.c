#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>


int main(int argc, char* argv[]) {
	
	
	int sock;
	struct sockaddr_in server;
	struct hostent *hp;
	char message[1024];
	
	printf("Enter message:  ");
	gets(message);

	sock = socket(AF_INET, SOCK_STREAM, 0);


	if (sock < 0) {
		printf("failed to create socket");
		exit(1);
	}
	
	server.sin_family = AF_INET;
	
	hp = gethostbyname("localhost");
	
	if (hp == 0) {
		printf("gethostbyname failed");
		exit(1);
	}
	

	memcpy(&server.sin_addr, hp->h_addr, hp->h_length);
	server.sin_port = htons(5000);

	
	if (connect(sock, (struct sockaddr *) &server, sizeof(server)) < 0) {
		printf("connect failed");
		close(sock);
		exit(1);
	}
	
	if (send(sock, message, sizeof(message), 0) < 0) {
		printf("send failed");
		close(sock);
		exit(1);
	}
	
	printf("You sent: %s\n", message);
	close(sock);
	
	return 0;
	
}