#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include "image.h"
#include "so_game_protocol.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"
#include "linked_list_mine.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define clear() system("clear");

typedef struct dataForAutentication{
	IdPacket*    id;
  	ImagePacket* mapTexture;
   	ImagePacket* Elevation;
  	ImagePacket* Image;
  	Image*		 text_for_serv;
  	PositionPacket* position;
  	int 	 	 socket;
  	int 		 autenticated;
}dataForAutentication;

void* autenticationHandler(dataForAutentication* data);

void* loginHandler(dataForAutentication* data);

void* registrationHandler(dataForAutentication* data);

dataForAutentication* newDataForAutentication();

int valid_username(linked_list* clients, char* usr); //returns 1 if valid

IdPacket* newIdPacket();

ImagePacket* newImagePacket(int id, Image* img, Type mode);

VehicleUpdatePacket* newVehicleUpdatePacket(int id, float r_f, float t_f);

WorldUpdatePacket* newWUP(World* world);

int newTCPConnection(char* ip, int port, struct sockaddr_in* str );

void sendTCPPacket(const PacketHeader* packet, int sock);


void sendUDPPacket(const PacketHeader* packet, int sock, struct sockaddr_in* happy);

PacketHeader* receiveAndDeserializeTCP(int sock);

PacketHeader* receiveAndDeserializeUDP(int sock, struct sockaddr_in* happy);

void updatePosition(const PacketHeader* packet, World* wrld, int id);

void addNewPlayer(const PacketHeader* packet, struct sockaddr_in* happy, World* wrld, int id, int sockTCP, int sockUDP);

int assign_ID(int* connected, int max_num);
