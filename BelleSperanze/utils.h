#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "image.h"
#include "so_game_protocol.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


IdPacket* newIdPacket();

ImagePacket* newImagePacket(int id, Image* img, Type mode);

VehicleUpdatePacket* newVehicleUpdatePacket(int id, float r_f, float t_f);

WorldUpdatePacket* newWUP(World* world);

int newTCPConnection(char* ip, int port, struct sockaddr_in* str );

void sendTCPPacket(const PacketHeader* packet, int sock);


void sendUDPPacket(const PacketHeader* packet, int sock, struct sockaddr_in* happy);

PacketHeader* receiveAndDeserializeTCP(int sock);

PacketHeader* receiveAndDeserializeUDP(int sock, struct sockaddr_in* happy);	

void updatePosition(const PacketHeader* packet, World* wrld, int id);

void addNewPlayer(const PacketHeader* packet, struct sockaddr_in* happy, World* wrld, int id, int sockTCP, int sockUDP);