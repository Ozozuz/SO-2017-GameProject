#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

void *client(){

	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock<0){
		printf("error creating socket\n");
		return 0;
	}

	struct sockaddr_in happy; 
	happy.sin_family = AF_INET;
	happy.sin_addr.s_addr = inet_addr("127.0.0.1");
	happy.sin_port = htons(27015);

	int ret;

	ret = connect(sock, (struct sockaddr*) &happy, sizeof(happy));
	if(ret<0){
		printf("connection error");
	}

	char msg[1024];

	printf("msg: ");
	scanf("%s",msg);
	ret = send(sock, msg, strlen(msg), 0);

	if(ret < 0){
		printf("error sending msg \n");
		return 0;
	}

	return NULL;
}

int main() {

	pthread_t thread;
	int num=0;

	while(1) {
		printf("creating client%d\n",num);
		if(pthread_create(&thread, NULL, client, NULL)){
			printf("Error create thread0;");
		}
		pthread_join(thread, NULL);
		num++;
	}

	return 0;
}