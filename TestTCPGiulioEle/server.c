#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


int main(){
	int sockets = socket(PF_INET, SOCK_STREAM, 0);
	if(socket<0){
		printf("Errore creazione socket\n" );
		return 0;
	}

	struct sockaddr_in happy;
	happy.sin_family = AF_INET;
	happy.sin_addr.s_addr = inet_addr("127.0.0.1");
	happy.sin_port = htons(27015);

	int ret;
	ret = bind(sockets, (struct sockaddr*) &happy, sizeof(happy));
	if (ret<0){
		printf("Errore binding socket\n" );
		return 0;
	}

	ret = listen(sockets,1);
	if (ret<0){
		printf("Errore listen socket\n" );
		return 0;
	}

	int socket_client;
	struct sockaddr_in sohappy;

	while(1){
		int dim = sizeof(sohappy);
		socket_client = accept(sockets, (struct sockaddr*) &sohappy, &dim);
		if (socket_client < 0){
			printf("Errore stabilimento connessione con client\n" );
			return 0;
		}

		char buf[1024];
		ret = recv(socket_client, buf, 1023, 0);
		if(ret<=0){
			printf("Errore nella ricezione \n" );
		}
		buf[ret] = '\0';
		printf("%s \n",buf );
		
		if (recv(socket_client, buf, sizeof(buf), MSG_PEEK | MSG_DONTWAIT) == 0){
			printf("Il client ha terminato.. esco\n");
			return 1;
		}
	}
	return 1;
}



