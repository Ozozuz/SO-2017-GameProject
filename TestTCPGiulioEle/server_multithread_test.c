#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <errno.h>
#include "common.h"

typedef struct{
    int socket;
    struct sockaddr_in* addr;
}handler_args_t;

void* connection_handler(void* arg) {
    handler_args_t* args = (handler_args_t*)arg;

    int sock=args->socket;
    struct sockaddr_in* client_addr=args->addr;

    int ret, recv_bytes;

    char buf[1024];
    size_t buf_len=sizeof(buf);
    size_t msg_len;

    char client_ip[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(client_addr->sin_addr), client_ip, INET_ADDRSTRLEN);
    uint16_t client_port = ntohs(client_addr->sin_port); // port number è unsigned short

    sprintf(buf, "Ciao, sono il server!\n");
    msg_len = strlen(buf);
    while ( (ret = send(sock, buf, msg_len, 0)) < 0 ) {
        if (errno == EINTR) continue;
        ERROR_HELPER(-1, "Errore nel mandare messaggio benvenuto");
    }
    
    while(1){
        //ricevo dal client
        while ( (recv_bytes = recv(sock, buf, buf_len, 0)) < 0 ) {
            if (errno == EINTR) continue;
            ERROR_HELPER(-1, "Errore nella ricezione lato server");
        }
        
        buf[recv_bytes]='\0';
        printf("[thread] messaggio ricevuto: %s\n", buf);
        
        //echo
        /*while ( (ret = send(sock, buf, recv_bytes, 0)) < 0 ) {
            if (errno == EINTR) continue;
            ERROR_HELPER(-1, "Cannot write to the socket");
        }*/
    }

    //questa parte non viene eseguita, per essere eseguita va gestito lo stop
    ret=close(sock);
    free(args->addr);
    free(args);
    printf("Thread creato per la connessione è terminato\n");
    pthread_exit(NULL);
}

int main(){
    int ret, server_sock, client_sock;
	server_sock=socket(AF_INET, SOCK_STREAM, 0);
	ERROR_HELPER(server_sock, "Errore crezione server_scoke");

	struct sockaddr_in server_addr;
	server_addr.sin_family=AF_INET;
	server_addr.sin_addr.s_addr=inet_addr("127.0.0.1");
	server_addr.sin_port=htons(27015);

	int len=sizeof(struct sockaddr_in);
    
	ret=bind(server_sock, (struct sockaddr*) &server_addr, len);
	ERROR_HELPER(ret, "Errore binding socket");

	ret=listen(server_sock, MAX_CONN_QUEUE);
	ERROR_HELPER(ret, "Errore listen socket");

	struct sockaddr_in* client_addr=calloc(1, sizeof(struct sockaddr_in));

	while(1){
		client_sock=accept(server_sock, (struct sockaddr*)client_addr, &len);
		ERROR_HELPER(client_sock, "Errore accept connessione");
        
        printf("Connessione accettata...\n");
        
        pthread_t thread;
        handler_args_t* thread_args=(handler_args_t*) malloc(sizeof(handler_args_t));
        thread_args->socket=client_sock;
        thread_args->addr=client_addr;
        
        ret=pthread_create(&thread, NULL, connection_handler, (void*) thread_args);
        PTHREAD_ERROR_HELPER(ret, "Errore nella creazione del thread");
        
        printf("E' stato creato un thread per gestire la connessione\n");
        
        ret=pthread_detach(thread);
        PTHREAD_ERROR_HELPER(ret, "Errore detach thread");
        
        client_addr=calloc(1, sizeof(struct sockaddr_in));
	}
	return 1;
}



