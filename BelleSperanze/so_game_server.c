
// #include <GL/glut.h> // not needed here
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"
#include "utils.h"
#include "common.h"

#define MAX_NUM_CLIENTS 10


typedef struct{
	int boh;
}thread_args;

World world;
Vehicle* vehicle;
int tcp_socket[MAX_NUM_CLIENTS];

void* server_udp(void* args){
	int ret;
	thread_args* arg=(thread_args*) args;
	int sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	ERROR_HELPER(sock, "Error creating udp socket");
	
	struct sockaddr_in addr, other_addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_port = htons(27016);
	
	ret=bind(sock, (struct sockaddr*) &addr, sizeof(addr));
	ERROR_HELPER(ret, "Error binding udp socket");
	
	Vehicle* vehicle_aux;
	char buf[1024];
	int len=sizeof(other_addr);
	
	PacketHeader* packet;
	VehicleUpdatePacket* v_update;
	ImagePacket* texture;
	IdPacket* id_packet;

	while(1){
		packet=receiveAndDeserializeUDP(sock, &other_addr);
		switch(packet->type){
			case GetTexture:	id_packet=(IdPacket*) packet; //il client mi sta chiedendo la texture di un giocatore, prima mi comunica il suo id
								//mi devo mettere in ascolto in tcp sulla socket di id_packet->id
								texture=(ImagePacket*) receiveAndDeserializeTCP(tcp_socket[id_packet->id]); //il client mi invia l'id di chi vuole la texture
								texture->header.type=PostTexture;
								texture->image=World_getVehicle(&world, texture->id)->texture;
								sendTCPPacket((const PacketHeader*) texture, tcp_socket[id_packet->id]);
								break;
			
			case VehicleUpdate:			v_update =(VehicleUpdatePacket*) packet;
										printf("[SERVER]Ho ricevuto forza rot=%f, trans=%f\n", v_update->rotational_force, v_update->translational_force);
										//vehicle_aux=World_getVehicle(&world, v_update->id);

										//vehicle_aux->translational_force_update=v_update->translational_force;
										//vehicle_aux->rotational_force_update=v_update->rotational_force;
										//sendUDPPacket((const PacketHeader*) newWUP(&world), sock, &other_addr);
										break;
		}
	}
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	if (argc<3) {
		printf("usage: %s <elevation_image> <texture_image>\n", argv[1]);
    	exit(-1);
  	}	
  	char* elevation_filename=argv[1];
  	char* texture_filename=argv[2];
  	char* vehicle_texture_filename="./images/arrow-right.ppm";
  	printf("loading elevation image from %s ... ", elevation_filename);
  	int ret;

  	// load the images
  	Image* surface_elevation = Image_load(elevation_filename);
  	if (surface_elevation) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}
	
	
  	printf("loading texture image from %s ... ", texture_filename);
  	Image* surface_texture = Image_load(texture_filename);
  	if (surface_texture) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}
	
  	printf("loading vehicle texture (default) from %s ... ", vehicle_texture_filename);
  	Image* vehicle_texture = Image_load(vehicle_texture_filename);
  	if (vehicle_texture) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}
  	
	World_init(&world, surface_elevation, surface_texture,  0.5, 0.5, 0.5);
    
	//UDP-part
	pthread_t thread;
	thread_args* args=(thread_args*) malloc(sizeof(thread_args));
	  	
	ret=pthread_create(&thread, NULL, server_udp, (void*) args);
    PTHREAD_ERROR_HELPER(ret, "Error creating thread");
	//pthread_join(thread, NULL);
	
  	//TCP-part
  	int id=1;
  	int sock=socket(AF_INET, SOCK_STREAM, 0);
  	ERROR_HELPER(sock, "Error creating socket");
  	
  	struct sockaddr_in server_addr;
  	server_addr.sin_family = AF_INET;
  	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  	server_addr.sin_port = htons(27015);
  	
  	int len=sizeof(struct sockaddr_in);
  	ret=bind(sock, (struct sockaddr*) &server_addr, len);
  	ERROR_HELPER(ret, "Error binding socket");
  	
  	ret=listen(sock, MAX_CONN_QUEUE);
  	ERROR_HELPER(ret, "Error listen socket");
  	
  	int client_sock;
  	struct sockaddr_in client_addr;
  	//in un while
	while(1){
		client_sock=accept(sock, (struct sockaddr*) &client_addr, (socklen_t*) &len);
	  	ERROR_HELPER(client_sock, "Error accept scoket");
		
		tcp_socket[id]=client_sock;
	  	//ricevo richiesta id
	  	IdPacket* id_packet=(IdPacket*) receiveAndDeserializeTCP(client_sock);
	  	id_packet->id=id;
	  	sendTCPPacket((const PacketHeader*) id_packet, client_sock);
		
      	
	  	ImagePacket* mapTexture=(ImagePacket*) receiveAndDeserializeTCP(client_sock);
	  	ImagePacket* mapElevation=(ImagePacket*) receiveAndDeserializeTCP(client_sock);
	  	ImagePacket* myImagePacket=(ImagePacket*) receiveAndDeserializeTCP(client_sock);
		
	  	mapTexture->header.type=PostTexture;
	  	mapTexture->id=0;
	  	mapTexture->image=surface_texture;
	  	sendTCPPacket((const PacketHeader*) mapTexture, client_sock);
		
	  	mapElevation->header.type=PostElevation;
	  	mapElevation->id=0;
	  	mapElevation->image=surface_elevation;
	  	sendTCPPacket((const PacketHeader*) mapElevation, client_sock);
		
	    vehicle=(Vehicle*) malloc(sizeof(Vehicle));
		Vehicle_init(vehicle, &world, id, myImagePacket->image);
		World_addVehicle(&world, vehicle);
		id++;
	}
  
  // not needed here
  //   // construct the world
	//World_init(&world, surface_elevation, surface_texture,  0.5, 0.5, 0.5);

  // // create a vehicle
  // vehicle=(Vehicle*) malloc(sizeof(Vehicle));
  // Vehicle_init(vehicle, &world, 0, vehicle_texture);

  // // add it to the world
  // World_addVehicle(&world, vehicle);


  
  // // initialize GL
  // glutInit(&argc, argv);
  // glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  // glutCreateWindow("main");

  // // set the callbacks
  // glutDisplayFunc(display);
  // glutIdleFunc(idle);
  // glutSpecialFunc(specialInput);
  // glutKeyboardFunc(keyPressed);
  // glutReshapeFunc(reshape);
  
  // WorldViewer_init(&viewer, &world, vehicle);

  
  // // run the main GL loop
  // glutMainLoop();

  // // check out the images not needed anymore
  // Image_free(vehicle_texture);
  // Image_free(surface_texture);
  // Image_free(surface_elevation);

  // // cleanup
	World_destroy(&world);
	return 0;             
}
