#include <pthread.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "so_game_protocol.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFF_DIM 1000000
#define UDP_SIZE 1024

typedef struct sockets{
	int tcp_sock;
	int udp_sock;
	int id;
} sockets;


int send_packet(const PacketHeader* packet, int sock){

	char buffer[BUFF_DIM];
	int ret;
	int size;

	size = Packet_serialize(buffer+sizeof(int), packet);
	memcpy(buffer, &size, 4);

	ret = send(sock, buffer, BUFF_DIM, 0);

	if(ret < 0){
		printf("error sending msg \n");
		return 0;
	}

	return 1;

}

PacketHeader* recive_packet(int sock){

	char buffer[BUFF_DIM];
	int ret;
	int size;

	ret = recv(sock, buffer, sizeof(buffer), MSG_WAITALL);
  	if(ret<=0){
		printf("error reciving msg \n");
		return 0;
	}

	memcpy(&size, buffer, 4);

	PacketHeader* packet = Packet_deserialize(buffer+sizeof(int), size);

	return packet;

}

PacketHeader* recive_packet_udp(int sock){

	char buffer[UDP_SIZE];
	int ret;
	int size;

	struct sockaddr_in client;
	int dim = sizeof(client);

	ret = recvfrom(sock, buffer, UDP_SIZE, 0,  (struct sockaddr*) &client, &dim );
	if (ret < 0){
		printf("error reciving msg\n");
		return 0;
	}

	memcpy(&size, buffer, 4);
	
	PacketHeader* packet = Packet_deserialize(buffer+sizeof(int), size);

	return packet;
}

void* udp_start(void* arguments){

	printf("[UDP] Started\n");

	sockets* socks = (sockets*) arguments;

 	while(1){
		PacketHeader* packet = recive_packet_udp(socks->udp_sock);

		switch (packet->type){
			case 0x1:{
				IdPacket* idp = (IdPacket*)packet;
				printf("PacketHeader type: %d from client id %d\n", packet->type, idp->id);
				break;
			}
			case 0x7:{
				VehicleUpdatePacket* vp = (VehicleUpdatePacket*)packet;
				printf("PacketHeader type: %d from client id %d\n", packet->type, vp->id);
				break;
			}
			default:{
				printf("No type\n");
				break;
			}
		}
	}

}


int main(int argc, char **argv){

	if (argc<3) {
    	printf("usage: %s <elevation_image> <texture_image>\n", argv[1]);
    	exit(-1);
  	}

  	char* elevation_filename=argv[1];
  	char* texture_filename=argv[2];

  	pthread_t client_thread;

  	printf("[SERVER] Server starting...\n");

  	printf("loading elevation image from %s ... ", elevation_filename);


  	Image* surface_elevation = Image_load(elevation_filename);
  	if (surface_elevation) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}


  	printf("loading texture image from %s ... ", texture_filename);
  	Image* surface_texture = Image_load(texture_filename);
  	if (surface_texture) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}

  	int id = 1;

	int tcp = socket(AF_INET, SOCK_STREAM, 0);
	if(tcp<0){
		printf("error creating socket\n");
		return 0;
	}

	struct sockaddr_in tcp_addr;
	tcp_addr.sin_family = AF_INET;
	tcp_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	tcp_addr.sin_port = htons(27015);

	int ret;
	ret = bind(tcp, (struct sockaddr*) &tcp_addr, sizeof(tcp_addr));
	if (ret<0){
		printf("error binding socket\n");
		return 0;
	}

	ret = listen(tcp,10);
	if (ret<0){
		printf("error listen socket\n");
		return 0;
	}

	int udp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if(udp<0){
		printf("error creating socket\n");
		return 0;
	}

	struct sockaddr_in udp_addr; 
	udp_addr.sin_family = AF_INET;
	udp_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	udp_addr.sin_port = htons(27014);

	ret = bind(udp, (struct sockaddr*) &udp_addr, sizeof(udp_addr));
	if (ret<0){
		printf("binding error\n");
		return 0;
	}

	sockets info;
	info.tcp_sock = tcp;
	info.udp_sock = udp;
	info.id = id;

	pthread_create(&client_thread, NULL, udp_start, (void*)&info);
	pthread_detach(client_thread);

	printf("[SERVER] Started\n");

	int socket_client;
	struct sockaddr_in client;
	int dim = sizeof(client);

	while(1){

		socket_client = accept(tcp, (struct sockaddr*) &client, &dim);
		if (socket_client < 0){
			printf("connection error\n");
			return 0;
		}

		printf("[SERVER] Client connected to the server\n");

		printf("[SERVER] Assigning client id and receive vehicle texture\n");

  		IdPacket* id_packet = (IdPacket*)recive_packet(socket_client);
  		id_packet->id = id;

  		send_packet((PacketHeader*)id_packet, socket_client);

  		ImagePacket* image_from_client = (ImagePacket*)recive_packet(socket_client);

  		Image_save(image_from_client->image, "imagefromclient.ppm");

  		printf("[SERVER] Sending server texture to the client...\n");

	  	ImagePacket* elevation = (ImagePacket*)malloc(sizeof(ImagePacket));
	  	PacketHeader e_head;
	  	e_head.type = PostElevation;
	  	elevation->header = e_head;
	  	elevation->image = surface_elevation;
	  	elevation->id = 0;
	  	send_packet((PacketHeader*)elevation, socket_client);

	  	ImagePacket* texture = (ImagePacket*)malloc(sizeof(ImagePacket));
	  	PacketHeader t_head;
	  	t_head.type = PostTexture;
	  	texture->header = t_head;
	  	texture->image = surface_texture;
	  	texture->id = 0;
	  	send_packet((PacketHeader*)texture, socket_client);

      	id++;
	}

	return 0;

}