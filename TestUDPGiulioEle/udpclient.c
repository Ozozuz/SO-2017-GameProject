#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(){

	int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sock<0){
		printf("Errore creazione socket\n");
		return 0;
	}

	struct sockaddr_in happy;

	happy.sin_family = AF_INET;
	happy.sin_addr.s_addr = inet_addr("127.0.0.1");
	happy.sin_port = htons(27015);

	while(1){
		int dim = sizeof(happy);
		char msg[1024];
		scanf("%s",msg);
		int ret = sendto(sock, msg, strlen(msg), 0, (struct sockaddr *) &happy, dim );
		if ( ret<0 ){
			printf("Errore invio msg\n" );
			return 0;
		}
	}
	return 0;
}