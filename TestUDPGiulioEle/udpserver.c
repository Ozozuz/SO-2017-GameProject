#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(){
	int ret;
	char buf[1024];
	int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	struct sockaddr_in sohappy;

	if(sock<0){
		printf("Errore creazione socket\n");
		return 0;
	}

	struct sockaddr_in happy; 
	happy.sin_family = AF_INET;
	happy.sin_addr.s_addr = inet_addr("127.0.0.1");
	happy.sin_port = htons(27015);

	ret = bind(sock, (struct sockaddr*) &happy, sizeof(happy));
	if (ret<0){
		printf("Errore binding socket\n" );
		return 0;
	}

	while(1){
		fflush(stdout);
		int dim = sizeof(sohappy);

		ret = recvfrom(sock, buf, 1024, 0,  (struct sockaddr *) &sohappy, &dim );
		if (ret < 0){
			printf("Errore ricezione dati\n" );
			return 0;
		}
		//[GIULIO]IMPORTANTE !
        printf("Received packet from %s:%d\n", inet_ntoa(sohappy.sin_addr), ntohs(sohappy.sin_port));
		buf[ret] = '\0';
		printf("%s \n",buf );	
	}
	return 0;
}