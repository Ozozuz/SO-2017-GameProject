#include <pthread.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <GL/glut.h>

#include "so_game_protocol.h"
#include "world_viewer.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFF_DIM 1000000
#define UDP_SIZE 1024

typedef struct {
  	volatile int run;
  	World* world;
  	int id;
  	int tcp;
  	int udp;
  	Image* my_texture;
}UpdaterArgs;

World world;
Vehicle* vehicle;
int my_id;


int connect_client(char* address, int port){

	int ret;

	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock<0){
		printf("error creating socket\n");
		return 0;
	}
 	
 	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(address);
	addr.sin_port = htons(port);

	ret = connect(sock, (struct sockaddr*) &addr, sizeof(addr));
	if(ret<0){
		printf("connection error");
		return 0;
	}

	return sock;

}

int send_packet(const PacketHeader* packet, int sock){

	char buffer[BUFF_DIM];
	int ret;
	int size;

	memset(buffer, 0, BUFF_DIM);

	size = Packet_serialize(buffer+sizeof(int), packet);
	memcpy(buffer, &size, 4);

	ret = send(sock, buffer, BUFF_DIM, 0);

	if(ret < 0){
		printf("error sending msg \n");
		return 0;
	}

	return 1;

}

int send_packet_udp(const PacketHeader* packet, int sock, struct sockaddr_in to){

	char buffer[UDP_SIZE];
	int ret;
	int size;

	memset(buffer, 0, UDP_SIZE);

	size = Packet_serialize(buffer+sizeof(int), packet);
	memcpy(buffer, &size, 4);

	int dim = sizeof(to);

	ret = sendto(sock, buffer, UDP_SIZE, 0, (struct sockaddr *)&to, dim );
	if(ret < 0){
		printf("error sending msg \n");
		return 0;
	}

	return 1;
}

PacketHeader* recive_packet(int sock){

	char buffer[BUFF_DIM];
	int ret;
	int size;

	ret = recv(sock, buffer, sizeof(buffer), MSG_WAITALL);
  	if(ret<=0){
		printf("error reciving msg \n");
		return 0;
	}

	memcpy(&size, buffer, 4);

	PacketHeader* packet = Packet_deserialize(buffer+sizeof(int), size);

	return packet;

}

PacketHeader* recive_packet_udp(int sock, struct sockaddr_in* from){

	char buffer[UDP_SIZE];
	int ret;
	int size;

	int dim = sizeof(from);

	ret = recvfrom(sock, buffer, UDP_SIZE, MSG_WAITALL,  (struct sockaddr*) from, &dim );
	if (ret < 0){
		printf("error reciving msg\n");
		return 0;
	}

	memcpy(&size, buffer, 4);

	PacketHeader* packet = Packet_deserialize(buffer+sizeof(int), size);

	return packet;
}

void* updater_thread(void* args){

	UpdaterArgs* uargs = (UpdaterArgs*) args;

	struct sockaddr_in server;
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_port = htons(27014);

  	while(uargs->run){

  		PacketHeader h;
  		h.type = VehicleUpdate;
  		VehicleUpdatePacket* updatepacket = (VehicleUpdatePacket*)malloc(sizeof(VehicleUpdatePacket));
  		updatepacket->header = h;
  		updatepacket->id = uargs->id;
  		updatepacket->rotational_force = vehicle->rotational_force_update;
  		updatepacket->translational_force = vehicle->translational_force_update;
  		send_packet_udp((PacketHeader*)updatepacket,uargs->udp, server);
  
    	WorldUpdatePacket* ups = (WorldUpdatePacket*)recive_packet_udp(uargs->udp, (struct sockaddr_in*) &server );

    	if (ups->num_vehicles > world.vehicles.size) {

    		printf("%d new player(s)\n", ups->num_vehicles-world.vehicles.size);

    		int i;
    		for (i=0;i<ups->num_vehicles;i++){

    			if (ups->updates[i].id != my_id && World_getVehicle(&world, ups->updates[i].id)==0) {

    				printf("Trovato\n");
    				//ask vehicle texture
    				IdPacket* get_text = (IdPacket*)malloc(sizeof(IdPacket));
  					PacketHeader id_head;
  					id_head.type = GetTexture;
  					get_text->header = id_head;
  					get_text->id = my_id;
  					send_packet_udp((PacketHeader*)get_text,uargs->udp, server);

  					get_text->id = ups->updates[i].id;
  					send_packet((PacketHeader*)get_text,uargs->tcp);

  					ImagePacket* v_text = (ImagePacket*)recive_packet(uargs->tcp);
  					printf("Ricevo Texture...\n");
  					char msg[20];
  					sprintf(msg,"%d_save_text_for_%d.ppm", my_id,ups->updates[i].id);
  					Image_save(v_text->image, msg);

  					Image* v_texture = (Image*) v_text->image;

  					printf("Immage rows %d cols %d channels %d\n", v_texture->rows, v_texture->cols, v_texture->channels);

  					Vehicle* v=(Vehicle*)malloc(sizeof(Vehicle));
  					Vehicle_init(v, &world, ups->updates[i].id, uargs->my_texture);

  					printf("Aggiunto\n");

  					World_addVehicle(&world, v);

  					Vehicle_applyTexture(v, v->texture);


    			}

    		}

    	}



    	int i;
    	for (i=0;i<ups->num_vehicles;i++){
    		//if (ups->updates[i].id != my_id){
    		Vehicle* v = World_getVehicle(&world, ups->updates[i].id);
    		v->x = ups->updates[i].x;
  			v->y = ups->updates[i].y;
  			v->theta = ups->updates[i].theta;
  			//}
    	}





    	World_update(uargs->world);
    	usleep(30000);

  	}

  	return 0;
}



int main(int argc, char **argv) {

	if (argc<3) {
		printf("usage: %s <server_address> <player texture>\n", argv[1]);
    	exit(-1);
    }

    printf("loading texture image from %s ... ", argv[2]);

  	Image* my_texture = Image_load(argv[2]);

  	if (my_texture) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}
  
  	char* address = argv[1];

  	int sock = connect_client(address, 27015);

  	////////

  	int sock_udp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sock_udp<0){
		printf("error creating socket\n");
		return 0;
	}

	/////////

	int ret;

	printf("[CLIENT] Connected to the server\n");

	printf("[CLIENT] Asking for id...\n");

  	IdPacket* id_packet = (IdPacket*)malloc(sizeof(IdPacket));
  	PacketHeader id_head;
  	id_head.type = GetId;
  	id_packet->header = id_head;
  	id_packet->id = -1;

  	send_packet((PacketHeader*)id_packet, sock);

  	id_packet = (IdPacket*)recive_packet(sock);

  	printf("[CLIENT] Getted ID: %d\n", id_packet->id);
  	my_id = id_packet->id;

  	//Packet_free((PacketHeader*)id_packet);

  	printf("[CLIENT] Sending my texture to the server...\n");
  	ImagePacket* my_image_for_server = (ImagePacket*)malloc(sizeof(ImagePacket));
  	PacketHeader my_image_head;
  	my_image_head.type = PostTexture;
  	my_image_for_server->header = my_image_head;
  	my_image_for_server->image = my_texture;
  	my_image_for_server->id = my_id;

  	send_packet((PacketHeader*)my_image_for_server, sock);

  	printf("[CLIENT] Reciving texture from the server...\n");

  	ImagePacket* elevation = (ImagePacket*)recive_packet(sock);
  	Image_save(elevation->image, "elevation.pgm");

  	ImagePacket* texture = (ImagePacket*)recive_packet(sock);
  	Image_save(texture->image, "texture.ppm");

	Image* surface_elevation = elevation->image;
  	Image* surface_texture = texture->image;

	World_init(&world, surface_elevation, surface_texture, 0.5, 0.5, 0.5);
  	vehicle=(Vehicle*)malloc(sizeof(Vehicle));
  	Vehicle_init(vehicle, &world, my_id, my_texture);
  	World_addVehicle(&world, vehicle);

  	//thread
  	pthread_t runner_thread;

  	UpdaterArgs runner_args;
  	runner_args.run=1;
  	runner_args.world=&world;
  	runner_args.id=my_id;
  	runner_args.udp=sock_udp;
  	runner_args.tcp=sock;
  	runner_args.my_texture=my_texture;

  	pthread_create(&runner_thread, NULL, updater_thread, &runner_args);
  	WorldViewer_runGlobal(&world, vehicle, &argc, argv);
  	runner_args.run=0;
  	void* retval;
  	pthread_join(runner_thread, &retval);

  	World_destroy(&world);

  	printf("[CLIENT] Disconnecting..\n");
  	close(sock);

	return 0;
}