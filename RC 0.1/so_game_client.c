#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"
#include "utils.h"

#define clear() system("clear");

int window;
int sockTCP;
int sockUDP;
int my_id;

WorldViewer viewer;
World world;
Vehicle* vehicle; // The vehicle

typedef struct{
  char* ipUDP;
  int portUDP;
  int id;
}thread_args;


void* UDPHandler(void* arg){
  thread_args* args = (thread_args*)arg;
  sockUDP = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  struct sockaddr_in* happy=malloc(sizeof(struct sockaddr_in));

  happy->sin_family = AF_INET;
  happy->sin_addr.s_addr = inet_addr(args->ipUDP);
  happy->sin_port = htons(args->portUDP);

  VehicleUpdatePacket* pack = newVehicleUpdatePacket(vehicle->id, vehicle->rotational_force_update, vehicle->translational_force );

  while(1){
    pack->rotational_force    =vehicle->rotational_force_update;
    pack->translational_force =vehicle->translational_force_update;
    sendUDPPacket((const PacketHeader*)pack, sockUDP, happy);
    WorldUpdatePacket* answer = (WorldUpdatePacket*) receiveAndDeserializeUDP(sockUDP, happy);
    addNewPlayer( (const PacketHeader*) answer, happy, &world, my_id, sockTCP, sockUDP);
    updatePosition((const PacketHeader*) answer, &world, my_id);
    usleep(30000);

  }
}

void disconnectionHandler(){
  shutdown(sockTCP,2);
  //close(sockUDP);
  printf("\n**Ho intercettato SIGINT**\n");
  exit(0);
}



void dWorldViewerisplay(void) {
  WorldViewer_draw(&viewer);
}


int main(int argc, char **argv) {
  if (argc<2) {
    printf("usage: %s <server_address>\n", argv[1]);
      exit(-1);
    }
    clear();


    int ret;
    char buf[1000000];
	

    //[Giulio]istanzione connessione TCP con server localhost, scanf ?
    int serverport = 27015;
    char* serverip = argv[1];
    struct sockaddr_in* happy=malloc(sizeof(struct sockaddr_in));
    sockTCP = newTCPConnection(serverip, serverport, happy);

    dataForAutentication* data = newDataForAutentication();
    data->socket = sockTCP;

    autenticationHandler(data);
    if(data->autenticated!=1){
    	printf("You used all your autentication attempts\nAutentication Failed\n");

    	Packet_free((PacketHeader*)data->id);
	  	Packet_free((PacketHeader*)data->mapTexture);
	   	Packet_free((PacketHeader*)data->Elevation);
	  	Packet_free((PacketHeader*)data->Image);
	  	Packet_free((PacketHeader*)data->text_for_serv);
	  	Packet_free((PacketHeader*)data->position);
	  	free(data);

    	return 0;
    }

  //these come from the server
  Image* map_texture   = data->mapTexture->image;
  Image* map_elevation = data->Elevation->image;
  Image* my_texture_from_server= data->Image->image;
  my_id = data->id->id;


  // construct the world
  World_init(&world, map_elevation, map_texture, 0.5, 0.5, 0.5);
  vehicle=(Vehicle*) malloc(sizeof(Vehicle));
  Vehicle_init(vehicle, &world, my_id, my_texture_from_server);

  if (data->position != NULL) {

    //printf("Le positioni ricevute sono: %f %f %f\n", data->position->x,data->position->y, data->position->theta);
    vehicle->x=data->position->x;
    vehicle->y=data->position->y;
    vehicle->theta=data->position->theta;
  }

  World_addVehicle(&world, vehicle);
  printf("[CLIENT]Mondo e veicolo creati\n" );

  pthread_t thread;
  thread_args* args = (thread_args*)malloc(sizeof(thread_args));
  args->portUDP     =27016;
  args->ipUDP  		=serverip;
  args->id     		= my_id;

  ret = pthread_create(&thread, NULL, UDPHandler, (void*)args);
  pthread_detach(thread);

  WorldViewer_runGlobal(&world, vehicle, &argc, argv);
  // cleanup
  
  World_destroy(&world);
  return 0;
}
