#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "common.h"

int main(){
    int ret;
	int sock = socket(PF_INET, SOCK_STREAM, 0);
	ERROR_HELPER(sock, "Errore creazione socket nel client");

	struct sockaddr_in addr; 
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_port = htons(27015);

	ret = connect(sock, (struct sockaddr*) &addr, sizeof(struct sockaddr_in));
	ERROR_HELPER(ret, "Errore connessione lato client");
    
    printf("Connessione stabilita!\n");
    
    char buf[1024];
    size_t buf_len = sizeof(buf);
    size_t msg_len;
    
    //ricevi messaggio benvenuto dal server
    while ( (msg_len = recv(sock, buf, buf_len - 1, 0)) < 0 ) {
        if (errno == EINTR) continue;
        ERROR_HELPER(-1, "Errore leggendo messaggio benvenuto");
    }
	buf[msg_len] = '\0';
    printf("%s", buf);
    
	while (1) {
        printf("Inserisci il tuo messaggio: ");

        /* Read a line from stdin
         *
         * fgets() reads up to sizeof(buf)-1 bytes and on success
         * returns the first argument passed to it. */
        if (fgets(buf, sizeof(buf), stdin) != (char*)buf) {
            fprintf(stderr, "Errore leggendo da stdin, esco..\n");
            exit(EXIT_FAILURE);
        }

        msg_len = strlen(buf);
        buf[--msg_len] = '\0'; // tolgo '\n' dalla fine del messaggio 

        //mando messaggio al server
        while ( (ret = send(sock, buf, msg_len, 0)) < 0) {
            if (errno == EINTR) continue;
            ERROR_HELPER(-1, "Errore invio messaggio al server");
        }

        //leggo messaggio echo dal server
        /*while ( (msg_len = recv(sock, buf, buf_len, 0)) < 0 ) {
            if (errno == EINTR) continue;
            ERROR_HELPER(-1, "Errore ricezione messaggio echo");
        }

        printf("Server response: %s\n", buf); // no need to insert '\0'*/
    }

    //non verrà eseguito, va gestito
    ret = close(sock);
    ERROR_HELPER(ret, "Errore chiusura socket");
    exit(EXIT_SUCCESS);
}
