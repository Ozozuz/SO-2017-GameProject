#include <stdio.h>
#include <stdlib.h>
#include "image.h"

typedef struct linked_list_node{
	int tcp_socket;
	int id;
    char* usr;
    char* psw;
    Image* texture;
    float x;
    float y;
    float theta;
	struct linked_list_node* next;
}linked_list_node;

typedef struct{
	int size;
	linked_list_node* head;
} linked_list;


linked_list* linked_list_new();

int linked_list_find(linked_list* l, int id);

linked_list_node* linked_list_get(linked_list* l, int id);

linked_list_node* linked_list_add(linked_list* l, int id, int tcp_socket, char* usr, char* psw);

void linked_list_set_texture(linked_list_node* node, Image* texture);

void linked_list_set_position(linked_list_node* node, float x, float y, float theta);

void linked_list_delete(linked_list* l, int id);

void linked_list_print(linked_list* l);

int linked_list_find_usr(linked_list* l, char* usr);
