
// #include <GL/glut.h> // not needed here
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"
#include "utils.h"
#include "common.h"
//#include "linked_list_mine.h"

#define MAX_NUM_CLIENTS 10

World world;
linked_list* clients_list; //contiene client connessi
linked_list* all_clients; //contiene client connessi e quelli connessi in passato
Image* surface_texture;
Image* surface_elevation;

Image* signIn(int sock, IdPacket* id_packet, Image* default_texture){
    int ret, ok;
    char usr[128], psw[128], *msg, *tmp, *tmp2;
    linked_list_node* node, aux;

    do{
        ret=recv(sock, usr, 128, 0);
        if (ret==0) return NULL;
        ERROR_HELPER(ret, "Error receiving username");

        ok=valid_username(all_clients, usr);

        if (ok) msg="OK";
        else msg="KO";

        ret=send(sock, msg, 3, 0);
        ERROR_HELPER(ret, "Error sending msg");

    } while(!ok);

    ret=recv(sock, psw, 128, MSG_WAITALL);
    if (ret==0) return NULL;
    ERROR_HELPER(ret, "Error receiving password");

    tmp =(char*) malloc(sizeof(char)*128); //importante sennò si bugga il campo usr di all_clients e clients_list (?)
    tmp2=(char*) malloc(sizeof(char)*128);
    tmp =strcpy(tmp, usr);
    tmp2=strcpy(tmp2, psw);

    printf("Si è connesso client con usr: %s\n", tmp);

    node=linked_list_add(all_clients, id_packet->id, sock, tmp, tmp2);
    linked_list_add(clients_list, id_packet->id, sock, tmp, tmp2);
    sendTCPPacket((const PacketHeader*) id_packet, sock);

    ImagePacket* mapTexture=(ImagePacket*) receiveAndDeserializeTCP(sock);
    ImagePacket* mapElevation=(ImagePacket*) receiveAndDeserializeTCP(sock);
    ImagePacket* clientImage=(ImagePacket*) receiveAndDeserializeTCP(sock); //se è NULL invio immagine di default

    //sleep(2);

    mapTexture->header.type=PostTexture;
    mapTexture->id=0;
    mapTexture->image=surface_texture;
    sendTCPPacket((const PacketHeader*) mapTexture, sock);

    mapElevation->header.type=PostElevation;
    mapElevation->id=0;
    mapElevation->image=surface_elevation;
    sendTCPPacket((const PacketHeader*) mapElevation, sock);

    Image* clientTexture;

    if (clientImage->header.type==GetTexture){
        printf("[SERVER] Non ho ricevuto una texture, invio quella di default\n");
        clientImage->header.type=PostTexture;
        clientImage->id=0;
        clientImage->image=default_texture;
        clientTexture=default_texture;
        sendTCPPacket((const PacketHeader*) clientImage, sock);
    }
    else{
        sendTCPPacket((const PacketHeader*) clientImage, sock);
        clientTexture = clientImage->image;
    }

    linked_list_set_texture(node, clientTexture);

    return clientTexture;
}

Image* login(int sock, IdPacket* id_packet){
    int ret;
    int old_id;
    char usr[128];
	char psw[128];
	char msg[128];
    linked_list_node* player;
    int maxtry = 3;

    while(maxtry){
        ret = recv(sock, usr, 128, MSG_WAITALL);
        if (ret==0) return NULL;
        ERROR_HELPER(sock, "Error receiving usr");

        ret = recv(sock, psw, 128, MSG_WAITALL);
        if (ret==0) return NULL;
        ERROR_HELPER(sock, "Error receiving psw");

        old_id = linked_list_find_usr(all_clients,usr);

        if (old_id == 0){
            //printf("usr not found\n");

            char* tmp = "usr";
            strcpy(msg,tmp);

            ret=send(sock, msg, 128, 0);
            ERROR_HELPER(ret, "Error sending msg");

            maxtry--;
            if(!maxtry) return NULL;
            continue;
        }

        player = linked_list_get(all_clients, old_id);

        if (linked_list_find(clients_list, old_id)) {
            //printf("player in game\n");

            char* tmp = "ingame";
            strcpy(msg,tmp);

            ret=send(sock, msg, 128, 0);
            ERROR_HELPER(ret, "Error sending msg");

            maxtry--;
            if(!maxtry) return NULL;
            continue;
        }

        if (!(strcmp(psw, player->psw)==0)){
            //printf("wrong psw\n");

            char* tmp = "psw";
            strcpy(msg,tmp);

            ret=send(sock, msg, 128, 0);
            ERROR_HELPER(ret, "Error sending msg");

            maxtry--;
            if(!maxtry) return NULL;
            continue;
        }
        break;
    }

    char* tmp="ok";
    strcpy(msg,tmp);

    ret=send(sock, msg, 128, 0);
    ERROR_HELPER(ret, "Error sending msg");
    printf("USR: %s autenticated!\n", usr);

	id_packet->id = old_id;

    sendTCPPacket((const PacketHeader*) id_packet, sock);

    ImagePacket* mapTexture=(ImagePacket*) receiveAndDeserializeTCP(sock);
    ImagePacket* mapElevation=(ImagePacket*) receiveAndDeserializeTCP(sock);

    mapTexture->header.type=PostTexture;
    mapTexture->id=0;
    mapTexture->image=surface_texture;
    sendTCPPacket((const PacketHeader*) mapTexture, sock);

    mapElevation->header.type=PostElevation;
    mapElevation->id=0;
    mapElevation->image=surface_elevation;
    sendTCPPacket((const PacketHeader*) mapElevation, sock);

    ImagePacket* playerTexture = newImagePacket(old_id, player->texture, PostTexture );
    sendTCPPacket((const PacketHeader*) playerTexture, sock);

    PacketHeader head;
	head.type=Position;

	PositionPacket* position = malloc(sizeof(PositionPacket));
	position->header=head;
	position->x=player->x;
	position->y=player->y;
	position->theta=player->theta;

	//printf("Le positioni ricevute sono: %f %f %f\n", position->x, position->y, position->theta);

	sendTCPPacket((const PacketHeader*) position, sock);

  Packet_free((PacketHeader*)position);

    linked_list_node* added = linked_list_add(clients_list,player->id, sock, player->usr, player->psw);

    linked_list_set_position(added, player->x, player->y, player->theta);

    return player->texture;
}

void* check_connection(void* args){
	int num_clients;
	Vehicle* v;
	linked_list_node* aux, *tmp;
	while(1){
		printf("controllo connessioni, ci sono %d veicoli\n",world.vehicles.size);
		num_clients=world.vehicles.size;
		aux=clients_list->head;
		while(aux!=NULL){
			if(recv(aux->tcp_socket, NULL, 0, MSG_PEEK | MSG_DONTWAIT) == 0){
				printf("Il client con id=%d si è disconnesso, libero risorse..\n", aux->id);
				v=World_getVehicle(&world, aux->id);
				World_detachVehicle(&world, v);

                tmp=linked_list_get(all_clients, aux->id);
                linked_list_set_position(tmp, v->x, v->y, v->theta);
				linked_list_delete(clients_list, aux->id);
			}
			aux=aux->next;
		}
		sleep(2);
	}
}

void* server_udp(void* args){
	int ret;
	int sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	ERROR_HELPER(sock, "Error creating udp socket");

	struct sockaddr_in addr, other_addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_port = htons(27016);

	ret=bind(sock, (struct sockaddr*) &addr, sizeof(addr));
	ERROR_HELPER(ret, "Error binding udp socket");

	Vehicle* vehicle_aux;
	char buf[1024];
	int len=sizeof(other_addr);

	PacketHeader* packet;
	VehicleUpdatePacket* v_update;
	ImagePacket* texture;
	IdPacket* id_packet;

	while(1){
		struct sockaddr_in other_addr;
		packet=receiveAndDeserializeUDP(sock, &other_addr);
		switch(packet->type){
			case GetTexture:
								id_packet=(IdPacket*) packet; //il client mi sta chiedendo la texture di un giocatore, prima mi comunica il suo id
								//mi devo mettere in ascolto in tcp sulla socket di id_packet->id
								linked_list_node* tmp=linked_list_get(clients_list, id_packet->id);
								texture=(ImagePacket*) receiveAndDeserializeTCP(tmp->tcp_socket); //il client mi invia l'id di chi vuole la texture
								texture->header.type=PostTexture;
								texture->image=World_getVehicle(&world, texture->id)->texture;
								sendTCPPacket((const PacketHeader*) texture, tmp->tcp_socket);
								break;

			case VehicleUpdate:
								v_update =(VehicleUpdatePacket*) packet;
								//printf("[SERVER]Ho ricevuto da id=%d forza rot=%f, trans=%f\n", v_update->id, v_update->rotational_force, v_update->translational_force);

								vehicle_aux=World_getVehicle(&world, v_update->id);
								vehicle_aux->translational_force_update=v_update->translational_force;
								vehicle_aux->rotational_force_update=v_update->rotational_force;

								World_update(&world);

								sendUDPPacket((const PacketHeader*) newWUP(&world), sock, &other_addr);
								break;

			default: break;
		}
	}
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	if (argc<3) {
		printf("usage: %s <elevation_image> <texture_image>\n", argv[1]);
    	exit(-1);
  	}
  	char* elevation_filename=argv[1];
  	char* texture_filename=argv[2];
  	char* vehicle_texture_filename="./images/30.ppm";
  	printf("loading elevation image from %s ... ", elevation_filename);
  	int ret;

  	// load the images
  	surface_elevation = Image_load(elevation_filename);
  	if (surface_elevation) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}


  	printf("loading texture image from %s ... ", texture_filename);
  	surface_texture = Image_load(texture_filename);
  	if (surface_texture) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}

  	printf("loading vehicle texture (default) from %s ... ", vehicle_texture_filename);
  	Image* vehicle_texture = Image_load(vehicle_texture_filename);
  	if (vehicle_texture) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}

	World_init(&world, surface_elevation, surface_texture,  0.5, 0.5, 0.5);


	clients_list=linked_list_new();
    all_clients=linked_list_new();

	//UDP-part
	pthread_t udp_thread, check_thread;

	ret=pthread_create(&udp_thread, NULL, server_udp, NULL);
    PTHREAD_ERROR_HELPER(ret, "Error creating thread");

	ret=pthread_create(&check_thread, NULL, check_connection, NULL);
    PTHREAD_ERROR_HELPER(ret, "Error creating thread");

  	//TCP-part
  	int id=1;

  	int sock=socket(AF_INET, SOCK_STREAM, 0);
  	ERROR_HELPER(sock, "Error creating socket");

  	struct sockaddr_in server_addr;
  	server_addr.sin_family = AF_INET;
  	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  	server_addr.sin_port = htons(27015);

  	int len=sizeof(struct sockaddr_in);
  	ret=bind(sock, (struct sockaddr*) &server_addr, len);
  	ERROR_HELPER(ret, "Error binding socket");

  	ret=listen(sock, MAX_CONN_QUEUE);
  	ERROR_HELPER(ret, "Error listen socket");

  	int client_sock, state;
  	struct sockaddr_in client_addr;
	Vehicle* vehicle;
    linked_list_node* node;

	while(1){
		client_sock=accept(sock, (struct sockaddr*) &client_addr, (socklen_t*) &len);
	  	ERROR_HELPER(client_sock, "Error accept scoket");

	  	//ricevo richiesta id
	  	IdPacket* id_packet=(IdPacket*) receiveAndDeserializeTCP(client_sock);
        if (id_packet==NULL) continue; //client si è sconnesso

        state=id_packet->id;
        id_packet->id=id;
        Image* clientTexture;

        if (state==-1)
            clientTexture=login(client_sock, id_packet); //state==-1 means client is logging in
        else if (state==0)
            clientTexture=signIn(client_sock, id_packet, vehicle_texture); //state==0 means client is signing in

        if (clientTexture==NULL) continue; //se è null significa che il client non è riuscito a loggare

		printf("Terminata connessione TCP con client\n");
	    vehicle=(Vehicle*) malloc(sizeof(Vehicle));
		Vehicle_init(vehicle, &world, id_packet->id, clientTexture);

		if (state==-1){
			linked_list_node* node = linked_list_get(clients_list,id_packet->id);
			vehicle->x=node->x;
			vehicle->y=node->y;
			vehicle->theta=node->theta;
		}

		World_addVehicle(&world, vehicle);
		printf("Aggiunto veicolo al mondo\n");

		if (state!=-1) id++;
	}

  // not needed here
  //   // construct the world
	//World_init(&world, surface_elevation, surface_texture,  0.5, 0.5, 0.5);

  // // create a vehicle
  // vehicle=(Vehicle*) malloc(sizeof(Vehicle));
  // Vehicle_init(vehicle, &world, 0, vehicle_texture);

  // // add it to the world
  // World_addVehicle(&world, vehicle);



  // // initialize GL
  // glutInit(&argc, argv);
  // glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  // glutCreateWindow("main");

  // // set the callbacks
  // glutDisplayFunc(display);
  // glutIdleFunc(idle);
  // glutSpecialFunc(specialInput);
  // glutKeyboardFunc(keyPressed);
  // glutReshapeFunc(reshape);

  // WorldViewer_init(&viewer, &world, vehicle);


  // // run the main GL loop
  // glutMainLoop();

  // // check out the images not needed anymore
        Image_free(vehicle_texture);
        Image_free(surface_texture);
        Image_free(surface_elevation);

  // // cleanup
	World_destroy(&world);
    return 0;
}
