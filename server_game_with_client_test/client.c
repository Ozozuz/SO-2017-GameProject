#include <pthread.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "so_game_protocol.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFF_DIM 1000000
#define UDP_SIZE 1024


int connect_client(char* address, int port){

	int ret;

	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock<0){
		printf("error creating socket\n");
		return 0;
	}
 	
 	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(address);
	addr.sin_port = htons(port);

	ret = connect(sock, (struct sockaddr*) &addr, sizeof(addr));
	if(ret<0){
		printf("connection error");
		return 0;
	}

	return sock;

}

int send_packet(const PacketHeader* packet, int sock){

	char buffer[BUFF_DIM];
	int ret;
	int size;

	memset(buffer, 0, BUFF_DIM);

	size = Packet_serialize(buffer+sizeof(int), packet);
	memcpy(buffer, &size, 4);

	ret = send(sock, buffer, BUFF_DIM, 0);

	if(ret < 0){
		printf("error sending msg \n");
		return 0;
	}

	return 1;

}

int send_packet_udp(const PacketHeader* packet, int sock){

	char buffer[UDP_SIZE];
	int ret;
	int size;

	memset(buffer, 0, UDP_SIZE);

	size = Packet_serialize(buffer+sizeof(int), packet);
	memcpy(buffer, &size, 4);

	struct sockaddr_in client_addr;
	client_addr.sin_family = AF_INET;
	client_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	client_addr.sin_port = htons(27014);
	int dim = sizeof(client_addr);

	ret = sendto(sock, buffer, UDP_SIZE, 0, (struct sockaddr *) &client_addr, dim );
	if(ret < 0){
		printf("error sending msg \n");
		return 0;
	}

	return 1;
}

PacketHeader* recive_packet(int sock){

	char buffer[BUFF_DIM];
	int ret;
	int size;

	ret = recv(sock, buffer, sizeof(buffer), MSG_WAITALL);
  	if(ret<=0){
		printf("error reciving msg \n");
		return 0;
	}

	memcpy(&size, buffer, 4);

	PacketHeader* packet = Packet_deserialize(buffer+sizeof(int), size);

	return packet;

}

PacketHeader* recive_packet_udp(int sock){

	char buffer[UDP_SIZE];
	int ret;
	int size;

	struct sockaddr_in client;
	int dim = sizeof(client);

	ret = recvfrom(sock, buffer, UDP_SIZE, 0,  (struct sockaddr*) &client, &dim );
	if (ret < 0){
		printf("error reciving msg\n");
		return 0;
	}

	memcpy(&size, buffer, 4);

	PacketHeader* packet = Packet_deserialize(buffer+sizeof(int), size);

	return packet;
}


int main(int argc, char **argv) {

	if (argc<3) {
		printf("usage: %s <server_address> <player texture>\n", argv[1]);
    	exit(-1);
    }

    printf("loading texture image from %s ... ", argv[2]);

  	Image* my_texture = Image_load(argv[2]);

  	if (my_texture) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}
  
  	int my_id;
  	char* address = argv[1];

  	int sock = connect_client(address, 27015);

  	////////

  	int sock_udp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sock_udp<0){
		printf("error creating socket\n");
		return 0;
	}

	/////////

	int ret;

	printf("[CLIENT] Connected to the server\n");

	printf("[CLIENT] Asking for id...\n");

  	IdPacket* id_packet = (IdPacket*)malloc(sizeof(IdPacket));
  	PacketHeader id_head;
  	id_head.type = GetId;
  	id_packet->header = id_head;
  	id_packet->id = -1;

  	send_packet((PacketHeader*)id_packet, sock);

  	id_packet = (IdPacket*)recive_packet(sock);

  	printf("[CLIENT] Getted ID: %d\n", id_packet->id);
  	my_id = id_packet->id;

  	//Packet_free((PacketHeader*)id_packet);

  	printf("[CLIENT] Sending my texture to the server...\n");
  	ImagePacket* my_image_for_server = (ImagePacket*)malloc(sizeof(ImagePacket));
  	PacketHeader my_image_head;
  	my_image_head.type = PostTexture;
  	my_image_for_server->header = my_image_head;
  	my_image_for_server->image = my_texture;
  	my_image_for_server->id = my_id;

  	send_packet((PacketHeader*)my_image_for_server, sock);

  	printf("[CLIENT] Reciving texture from the server...\n");

  	ImagePacket* elevation = (ImagePacket*)recive_packet(sock);
  	Image_save(elevation->image, "elevation.pgm");

  	ImagePacket* texture = (ImagePacket*)recive_packet(sock);
  	Image_save(texture->image, "texture.ppm");

  	////////UDP

  	printf("[CLIENT] Testing UDP...\n");

  	VehicleUpdatePacket* vehicle_packet = (VehicleUpdatePacket*)malloc(sizeof(VehicleUpdatePacket));
  	PacketHeader v_head;
  	v_head.type = VehicleUpdate;
  	vehicle_packet->header = v_head;
  	vehicle_packet->id = my_id;
  	vehicle_packet->rotational_force = 9.0;
  	vehicle_packet->translational_force = 9.0;

	while(1){
		send_packet_udp((PacketHeader*)id_packet, sock_udp);
		sleep(5);
		send_packet_udp((PacketHeader*)vehicle_packet, sock_udp);
	}

  	printf("[CLIENT] Disconnecting..\n");
  	close(sock);

	return 0;
}