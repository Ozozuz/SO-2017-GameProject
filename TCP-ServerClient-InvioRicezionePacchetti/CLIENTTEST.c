#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "so_game_protocol.h"
#include <math.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(){
	int sockets = socket(PF_INET, SOCK_STREAM, 0);
	if(sockets<0){
		printf("Errore creazione socket\n");
		return 0;
	}

	struct sockaddr_in happy; 
	happy.sin_family = AF_INET;
	happy.sin_addr.s_addr = inet_addr("127.0.0.1");
	happy.sin_port = htons(27015);

	int ret;
	char buffer[1000000];
	int size;

    char* vehicle_texture_filename="./images/arrow-right.ppm";
    Image* vehicle_texture = Image_load(vehicle_texture_filename);


    PacketHeader p2;
    ImagePacket* img = (ImagePacket*)malloc(sizeof(ImagePacket));

    p2.type = 0x4;
   // p2.size = sizeof(vehicle_texture);
    img->header = p2;
    img->image  = vehicle_texture;
    img->id     = 666;

	PacketHeader p;
	IdPacket* id = (IdPacket*)malloc(sizeof(IdPacket));

	p.type = 0x1;
	id->id = 69;
	id->header = p;
	
	int* intposition = (int*)(buffer);

	//size = Packet_serialize(buffer, (const PacketHeader*)id);
	size= Packet_serialize(buffer+sizeof(int), (const PacketHeader*)img);

	printf("L' immagine e' grande %d \n",size );
	*intposition = size;

	ret = connect(sockets, (struct sockaddr*) &happy, sizeof(happy));
	if(ret<0){
		printf("Errore connessione lato client");
	}

	ret = send(sockets, buffer, size, 0);
	if(ret != size){
		printf("Errore nell' invio \n");
		return 0;
	}

	//char* test = "lollmaoxd";
	//ret = send(sockets, test, strlen(test), 0);
	//printf("[CLIENT] Ho inviato %d byte\n",ret );
	return 1;
}
