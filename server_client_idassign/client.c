#include <pthread.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "so_game_protocol.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


int send_packet(const char* packet, int sock){

	int ret;

	ret = send(sock, packet, 1000000, 0);

	if(ret < 0){
		printf("error sending msg \n");
		return 0;
	}

	return 1;

}


int main() {

	//pthread_t listen_thread;
	char buff[1000000];

	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock<0){
		printf("error creating socket\n");
		return 0;
	}

	struct sockaddr_in happy; 
	happy.sin_family = AF_INET;
	happy.sin_addr.s_addr = inet_addr("127.0.0.1");
	happy.sin_port = htons(27015);

	int ret;

	ret = connect(sock, (struct sockaddr*) &happy, sizeof(happy));
	if(ret<0){
		printf("connection error");
		return 0;
	}

	printf("[CLIENT] Connected to the server\n");

	printf("[CLIENT] Asking for id...\n");

  	IdPacket* id_packet = (IdPacket*)malloc(sizeof(IdPacket));

  	PacketHeader id_head;
  	id_head.type = GetId;

  	id_packet->header = id_head;

  	char id_packet_buffer[1000000];
  	memset(id_packet_buffer, 0, sizeof(id_packet_buffer));

  	int id_packet_buffer_size = Packet_serialize(&id_packet_buffer[4], &id_packet->header);

  	memcpy(id_packet_buffer, &id_packet_buffer_size, 4);

  	send_packet(id_packet_buffer, sock);

  	ret = recv(sock, buff, sizeof(buff), MSG_WAITALL);
  	if(ret<=0){
		printf("error reciving msg \n");
		return 0;
	}


  	memcpy(&id_packet_buffer_size, buff, 4);

  	id_packet = (IdPacket*)Packet_deserialize(&buff[4], id_packet_buffer_size);

  	printf("[CLIENT] Getted ID: %d\n", id_packet->id);

  	printf("[CLIENT] Disconnecting..\n");

  	close(sock);

	return 0;
}