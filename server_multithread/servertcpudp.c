#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

void *tcp(){

	printf("thread0: SERVER TCP\n\n");

	int tcp = socket(AF_INET, SOCK_STREAM, 0);
	if(tcp<0){
		printf("error creating socket\n");
		return 0;
	}

	struct sockaddr_in tcp_addr;
	tcp_addr.sin_family = AF_INET;
	tcp_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	tcp_addr.sin_port = htons(27015);

	char tcp_buff[1024];

	int ret;
	ret = bind(tcp, (struct sockaddr*) &tcp_addr, sizeof(tcp_addr));
	if (ret<0){
		printf("error binding socket\n");
		return 0;
	}

	ret = listen(tcp,10);
	if (ret<0){
		printf("error listen socket\n");
		return 0;
	}

	int socket_client;
	struct sockaddr_in client;
	int dim = sizeof(client);

	while(1){

		socket_client = accept(tcp, (struct sockaddr*) &client, &dim);
		if (socket_client < 0){
			printf("connection error\n");
			return 0;
		}

		memset(tcp_buff, 0, sizeof(tcp_buff));

		ret = recv(socket_client, tcp_buff, sizeof(tcp_buff), 0);
		if(ret<=0){
			printf("error reciving msg \n");
		}

		printf("tcp_message: %s \n",tcp_buff);

	}

	return NULL;

}

void *udp(){

	printf("thread1: SERVER UDP\n\n");

	int ret;
	char udp_buff[1024];
	int udp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if(udp<0){
		printf("error creating socket\n");
		return 0;
	}

	struct sockaddr_in udp_addr; 
	udp_addr.sin_family = AF_INET;
	udp_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	udp_addr.sin_port = htons(27014);

	ret = bind(udp, (struct sockaddr*) &udp_addr, sizeof(udp_addr));
	if (ret<0){
		printf("binding error\n");
		return 0;
	}

	struct sockaddr_in client;
	int dim = sizeof(client);

	while(1){

		fflush(stdout);

		memset(udp_buff, 0, sizeof(udp_buff));

		ret = recvfrom(udp, udp_buff, 1024, 0,  (struct sockaddr*) &client, &dim );
		if (ret < 0){
			printf("error reciving msg\n");
			return 0;
		}
		printf("udp_message: %s \n",udp_buff);	
	}

	return NULL;
}


int main() {

	pthread_t thread0, thread1;

	if(pthread_create(&thread0, NULL, tcp, NULL)){
		printf("Error create thread0;");
	}

	if(pthread_create(&thread1, NULL, udp, NULL)){
		printf("Error create thread1;");
	}

	pthread_join(thread0, NULL);
	pthread_join(thread1, NULL);

	return 0;

}