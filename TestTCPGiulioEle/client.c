#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(){
	int sockets = socket(PF_INET, SOCK_STREAM, 0);
	if(sockets<0){
		printf("Errore creazione socket\n");
		return 0;
	}

	struct sockaddr_in happy; 
	happy.sin_family = AF_INET;
	happy.sin_addr.s_addr = inet_addr("127.0.0.1");
	happy.sin_port = htons(27015);

	int ret;

	ret = connect(sockets, (struct sockaddr*) &happy, sizeof(happy));
	if(ret<0){
		printf("Errore connessione lato client");
	}

	char* stringaditest = "elelo22badgirl\n";
	int len = strlen(stringaditest);

	ret = send(sockets, stringaditest, len, 0);
	if(ret != len){
		printf("Errore nell' invio \n");
		return 0;
	}
	
	close(sockets);
	return 1;
}