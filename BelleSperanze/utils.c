#include "utils.h"

IdPacket* newIdPacket(){
	PacketHeader head;
	IdPacket* packet = (IdPacket*)malloc(sizeof(IdPacket));

	head.type = GetId;
	packet->header = head;
	packet->id = -1;

	return packet;
}

ImagePacket* newImagePacket(int id, Image* img, Type mode){
	PacketHeader head;
	ImagePacket* packet = (ImagePacket*)malloc(sizeof(ImagePacket));

	head.type = mode;
	packet->id = id;
	packet->image = img;
	packet->header  = head;

	return packet;
}

VehicleUpdatePacket* newVehicleUpdatePacket(int id, float r_f, float t_f){
	
	PacketHeader head;
	VehicleUpdatePacket* packet = (VehicleUpdatePacket*)malloc(sizeof(VehicleUpdatePacket));

	head.type = VehicleUpdate;
	packet->header = head;
	packet->id     = id;
	packet->rotational_force = r_f;
	packet->translational_force = t_f;

	return packet;
}

WorldUpdatePacket* newWUP(World* world){
	WorldUpdatePacket* w_packet=(WorldUpdatePacket*) malloc(sizeof(WorldUpdatePacket));
	
	PacketHeader head;
	head.type=WorldUpdate;
	w_packet->header=head;
	
	w_packet->num_vehicles=world->vehicles.size;
	w_packet->updates=(ClientUpdate*) malloc(sizeof(ClientUpdate)*(w_packet->num_vehicles));
	
	Vehicle* aux;
	for(int i=0; i<w_packet->num_vehicles; i++){
		aux=World_getVehicle(world, i+1);
		w_packet->updates[i].id=aux->id;
		w_packet->updates[i].x=aux->x;
		w_packet->updates[i].y=aux->y;
		w_packet->updates[i].theta=aux->theta;
	}
	return w_packet;
}

int newTCPConnection(char* ip, int port, struct sockaddr_in* str ){
	int sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(sock<0){
			printf("[CLIENT]Errore creazione socket\n");
			return 0;
	}
	int ret;
	int dim = sizeof(*str);
	str->sin_family = AF_INET;
	str->sin_addr.s_addr = inet_addr(ip);
	str->sin_port = htons(port);
	ret = connect(sock, (struct sockaddr*) str, dim);
	if(ret<0){
		printf("Errore connessione lato client");
	}
	return sock;
}

void sendTCPPacket(const PacketHeader* packet, int sock){
	printf("Sono dentro sendTCPPacket\n");
	char buf[1000000];
	int* intposition = (int*)(buf);
	int ret;
	int size;
	size = Packet_serialize(buf+sizeof(int), packet);
	*intposition = size;
	ret = send(sock, buf, 1000000, 0);
	if(ret != 1000000){
		printf("Errore nell' invio \n");
		return ;
	}
	return;
}

void sendUDPPacket(const PacketHeader* packet, int sock, struct sockaddr_in* happy){

	//TODO: 1mb è troppo
	char buf[1024];
	int dim = sizeof(struct sockaddr_in);

	int* intposition = (int*)(buf);
	int size;

	size = Packet_serialize(buf+sizeof(int), packet);
	*intposition = size;

	sendto(sock, buf, 1024, 0,(struct sockaddr*)happy, dim );
}

PacketHeader* receiveAndDeserializeTCP(int sock){
	printf("Sono dentro receiveAndDeserializeTCP\n");
	char buf[1000000];
	int ret;
	PacketHeader* ris;
	int* packetsize = (int*)malloc(sizeof(int));

	ret = recv(sock, buf, 1000000, MSG_WAITALL);
	if(ret<=0){
		printf("Errore nella ricezione \n" );
	}
	memcpy(packetsize, buf, 4);
	ris = Packet_deserialize(buf+4,*packetsize);

	return ris;
}	

PacketHeader* receiveAndDeserializeUDP(int sock, struct sockaddr_in* happy){
	char buf[1024];
	int ret;
	int dim = sizeof(struct sockaddr_in);

	PacketHeader* ris;
	int* packetsize = (int*)malloc(sizeof(int));
	ret = recvfrom(sock, buf, 1024, 0, (struct sockaddr*) happy, (socklen_t*) &dim);
	if(ret<=0){
		printf("Errore nella ricezione \n" );
	}
	memcpy(packetsize, buf, 4);
	ris = Packet_deserialize(buf+4,*packetsize);

	return ris;
}

void updatePosition(const PacketHeader* packet, World* wrld, int id){
	int i;
	WorldUpdatePacket* pack = ( WorldUpdatePacket* )packet;
	int servernumveic = pack->num_vehicles;
	for(i=0; i<servernumveic; i++){
		if(pack->updates[i].id != id){
			printf("[CLIENT]Aggiorno la mia pos con dati del server\n");
			ClientUpdate pos = pack->updates[i];
			Vehicle* tmp     = World_getVehicle(wrld, pos.id);
			tmp->x = pos.x;
			tmp->y = pos.y;
			//tmp->z = pos.z;
			tmp->theta = pos.theta;
		}
	}
}

void addNewPlayer(const PacketHeader* packet, struct sockaddr_in* happy, World* wrld, int id, int sockTCP, int sockUDP){
	WorldUpdatePacket* pack = ( WorldUpdatePacket* )packet;
	int servernumveic = pack->num_vehicles;
	int mynumveic     = wrld->vehicles.size;
	int i;

	if(mynumveic<servernumveic){
		printf("[CLIENT]E' arrivato un nuovo giocatore !\n");
		for(i=0; i<servernumveic; i++){
			ClientUpdate tmp = pack->updates[i];
			if( (World_getVehicle( wrld, tmp.id) == 0) ){
				printf("Sono id=%d e sto aggiungendo id=%d\n", id, tmp.id);
				IdPacket* reqInfo    = newIdPacket();
				reqInfo->id          = id;
				reqInfo->header.type = GetTexture;
				ImagePacket* req     = newImagePacket(tmp.id, NULL, GetTexture);

				sendUDPPacket( (const PacketHeader*)reqInfo, sockUDP, happy);
				printf("Ho inviato pacchetto Id al server..\n");
				sendTCPPacket( (const PacketHeader*)req, sockTCP );
				printf("Ho inviato il pacchetto tcp al sever per avere l'immagine di id=%d\n", tmp.id);
				ImagePacket* answer  = (ImagePacket*)receiveAndDeserializeTCP(sockTCP);

				Vehicle* new_vehicle=(Vehicle*) malloc(sizeof(Vehicle));
				Vehicle_init(new_vehicle, wrld, answer->id ,answer->image );
				World_addVehicle(wrld, new_vehicle);
			}
		}
	}
}