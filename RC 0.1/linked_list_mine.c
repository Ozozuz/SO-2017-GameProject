#include "linked_list_mine.h"
#include "string.h"

linked_list* linked_list_new(){
	linked_list* ris=(linked_list*) malloc(sizeof(linked_list));
	ris->size=0;
	ris->head=NULL;
	return ris;
}

int linked_list_find(linked_list* l, int id){ 
	if (l==NULL) return 0;
	linked_list_node* aux=l->head;
	while(aux!=NULL){
		if (aux->id==id) return 1;
		aux=aux->next;
	}
	return 0;
}

linked_list_node* linked_list_get(linked_list* l, int id){
	if (!linked_list_find(l, id)) return NULL;

	linked_list_node* aux=l->head;
	while(aux!=NULL){
		if (aux->id==id) return aux;
		aux=aux->next;
	}
	return NULL;
}

linked_list_node* linked_list_add(linked_list* l, int id, int tcp_socket, char* usr, char* psw){
	if (l==NULL) return NULL;
	linked_list_node* ris=(linked_list_node*) malloc(sizeof(linked_list_node));
	ris->id=id;
	ris->tcp_socket=tcp_socket;
    ris->usr=usr;
    ris->psw=psw;
	ris->next=l->head;
	l->head=ris;
	l->size+=1;
	return ris;
}

void linked_list_set_texture(linked_list_node* node, Image* texture){
    node->texture=texture;
}

void linked_list_set_position(linked_list_node* node, float x, float y, float theta){
    node->x=x;
    node->y=y;
    node->theta=theta;
}

void linked_list_delete(linked_list* l, int id){
	if (!linked_list_find(l, id)) return;

	linked_list_node* aux=l->head;
	l->size-=1;

	if (aux->id==id){
		l->head=aux->next;
		free(aux);
		return;
	}

	while(aux->next!=NULL){
		if (aux->next->id==id){
			aux->next=aux->next->next;
			return;
		}
		aux=aux->next;
	}
}

void linked_list_print(linked_list* l){
	if (l==NULL) return;
	linked_list_node* aux=l->head;
	while(aux!=NULL){
		printf("%d ", aux->id);
		aux=aux->next;
	}
	printf("\n");
}

int linked_list_find_usr(linked_list* l, char* usr){
	if (l==NULL) return 0;
	linked_list_node* aux=l->head;
	while(aux!=NULL){
		if (strcmp(aux->usr, usr)==0) return aux->id;
		aux=aux->next;
	}
	return 0;
}

/*int main(){
	linked_list* l=linked_list_new();
	printf("Dim lista=%d\n", l->size);
	linked_list_add(l, 1, 3);
	printf("Dim lista=%d\n", l->size);
	linked_list_add(l, 2, 5);
	printf("Dim lista=%d\n", l->size);
	linked_list_add(l, 3, 7);
	printf("Dim lista=%d\n", l->size);
	linked_list_print(l);
	linked_list_delete(l, 2);
	linked_list_print(l);
	printf("Dim lista=%d\n", l->size);
	printf("Sto cercando id=1, risultato è %d\n", linked_list_find(l, 1));
	printf("Prvo get, id=3, tcp=%d\n", linked_list_get(l, 3)->tcp_socket);
	linked_list_print(l);
	linked_list_delete(l, 3);
	linked_list_print(l);
	linked_list_delete(l, 1);
	linked_list_print(l);
	return 1;
}*/
