#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"
#include "utils.h"

int window;
int sockTCP;
int sockUDP;
int my_id;

WorldViewer viewer;
World world;
Vehicle* vehicle; // The vehicle

typedef struct{
	char* ipUDP;
	int portUDP;
  int id;
}thread_args;


void* UDPHandler(void* arg){
	thread_args* args = (thread_args*)arg;
	sockUDP = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	struct sockaddr_in* happy=malloc(sizeof(struct sockaddr_in)); 

	happy->sin_family = AF_INET;
	happy->sin_addr.s_addr = inet_addr(args->ipUDP);
	happy->sin_port = htons(args->portUDP);

	VehicleUpdatePacket* pack = newVehicleUpdatePacket(vehicle->id, vehicle->rotational_force_update, vehicle->translational_force );
	
	while(1){
		pack->rotational_force    =vehicle->rotational_force_update;
		pack->translational_force =vehicle->translational_force_update;
		sendUDPPacket((const PacketHeader*)pack, sockUDP, happy);
   	//printf("[CLIENT] Sono id=%d, invio al server vettori forza rot=%f, trans=%f\n", my_id, pack->rotational_force, pack->translational_force);
		WorldUpdatePacket* answer = (WorldUpdatePacket*) receiveAndDeserializeUDP(sockUDP, happy);
		printf("[CLIENT]Invio rot=%f, trans=%f, ci sono %d player e ricevo id %d\n",pack->rotational_force, pack->translational_force, answer->num_vehicles, my_id);
		addNewPlayer( (const PacketHeader*) answer, happy, &world, my_id, sockTCP, sockUDP);
		updatePosition((const PacketHeader*) answer, &world, my_id);
    usleep(30000);

	}
}



///////////////////////////////////////////////////////////////////

/*void keyPressed(unsigned char key, int x, int y)
{
  switch(key){
  case 27:
    glutDestroyWindow(window);
    exit(0);
  case ' ':
    vehicle->translational_force_update = 0;
    vehicle->rotational_force_update = 0;
    break;
  case '+':
    viewer.zoom *= 1.1f;
    break;
  case '-':
    viewer.zoom /= 1.1f;
    break;
  case '1':
    viewer.view_type = Inside;
    break;
  case '2':
    viewer.view_type = Outside;
    break;
  case '3':
    viewer.view_type = Global;
    break;
  }
}


void specialInput(int key, int x, int y) {
  switch(key){
  case GLUT_KEY_UP:
    vehicle->translational_force_update += 0.1;
    break;
  case GLUT_KEY_DOWN:
    vehicle->translational_force_update -= 0.1;
    break;
  case GLUT_KEY_LEFT:
    vehicle->rotational_force_update += 0.1;
    break;
  case GLUT_KEY_RIGHT:
    vehicle->rotational_force_update -= 0.1;
    break;
  case GLUT_KEY_PAGE_UP:
    viewer.camera_z+=0.1;
    break;
  case GLUT_KEY_PAGE_DOWN:
    viewer.camera_z-=0.1;
    break;
  }
}*/


void dWorldViewerisplay(void) {
  WorldViewer_draw(&viewer);
}
/*

void reshape(int width, int height) {
  WorldViewer_reshapeViewport(&viewer, width, height);
}

void idle(void) {
  World_update(&world);
  usleep(30000);
  glutPostRedisplay();
  
  // decay the commands
  vehicle->translational_force_update *= 0.999;
  vehicle->rotational_force_update *= 0.7;
}*/

int main(int argc, char **argv) {
	if (argc<3) {
		printf("usage: %s <server_address> <player texture>\n", argv[1]);
    	exit(-1);
    }
    printf("loading texture image from %s ... ", argv[2]);
  	Image* my_texture = Image_load(argv[2]);
  	if (my_texture) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}
  
  	Image* my_texture_for_server; // chi è ? Non dovrebbe essere proprio my_texture ?
  	// todo: connect to the server
  	//   -get ad id
  	//   -send your texture to the server (so that all can see you)ImagePacket
  	//   -get an elevation map
  	//   -get the texture of the surface
  	
  	my_texture_for_server = my_texture;
  	int ret;
  	char buf[1000000];


  	//[Giulio]istanzione connessione TCP con server localhost, scanf ?
  	int serverport = 27015;
  	char* serverip = "127.0.0.1";
  	struct sockaddr_in* happy=malloc(sizeof(struct sockaddr_in)); 

  	sockTCP = newTCPConnection(serverip, serverport, happy);

	//[Giulio]Preparo pacchetti da inviare al server
	IdPacket* askForId;
	ImagePacket* askForTexture;
	ImagePacket* askForElevation;
	ImagePacket* sendImage;

	//[Giulio]Ed i pacchetti che mi aspetto di ricevere
	IdPacket* assignedID;
	ImagePacket* mapTexture;
	ImagePacket* mapElevation;
	ImagePacket* myImagePacket;

	//[Giulio]Inizio ad inviare i pacchetti, IDPacket per primo perché mi serve il suo valore dal server;
	askForId = newIdPacket();
	sendTCPPacket((const PacketHeader*) askForId, sockTCP);

	printf("[CLIENT] ho inviato richiesta id con pacchetto->id=%d\n", askForId->id);
	assignedID = (IdPacket*) receiveAndDeserializeTCP(sockTCP);
	my_id =  assignedID->id;
	printf("[CLIENT] ho ricevuto id %d\n", my_id);

	//[Giulio] Creo i pacchetti di richiesta ed invio immagini
	askForTexture  = newImagePacket(my_id, NULL, GetTexture);
	askForElevation= newImagePacket(my_id, NULL, GetElevation);
	sendImage      = newImagePacket(my_id, my_texture_for_server, PostTexture); 
  printf("[CLIENT] Ho creato immagini\n");

	sendTCPPacket((const PacketHeader*)askForTexture, sockTCP);
	sendTCPPacket((const PacketHeader*)askForElevation, sockTCP);
	sendTCPPacket((const PacketHeader*)sendImage, sockTCP);
  
	mapTexture     = (ImagePacket*) receiveAndDeserializeTCP(sockTCP);
	mapElevation   = (ImagePacket*) receiveAndDeserializeTCP(sockTCP);

	//these come from the server
  Image* map_texture   = mapTexture->image;
	Image* map_elevation = mapElevation->image;
	Image* my_texture_from_server = my_texture_for_server;


	// construct the world
	World_init(&world, map_elevation, map_texture, 0.5, 0.5, 0.5);
	vehicle=(Vehicle*) malloc(sizeof(Vehicle));
	Vehicle_init(vehicle, &world, my_id, my_texture_from_server);
	World_addVehicle(&world, vehicle);

	 // spawn a thread that will listen the update messages from
	 // the server, and sends back the controls
	 // the update for yourself are written in the desired_*_force
	 // fields of the vehicle variable
	 // when the server notifies a new player has joined the game
	 // request the texture and add the player to the pool
	 /*FILLME*/
	
  pthread_t thread;
	thread_args* args = (thread_args*)malloc(sizeof(thread_args));
	args->portUDP=27016;
	args->ipUDP  =serverip;
  args->id     = my_id;

	ret = pthread_create(&thread, NULL, UDPHandler, (void*)args);
	pthread_detach(thread);

	WorldViewer_runGlobal(&world, vehicle, &argc, argv);
	// cleanup
	World_destroy(&world);
	return 0;             
}
