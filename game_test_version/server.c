#include <pthread.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "so_game_protocol.h"
#include "world_viewer.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFF_DIM 1000000
#define UDP_SIZE 1024
#define NUM_PLAYER 10

typedef struct{
	int tcp_sock;
	int udp_sock;
	int id;
} sockets;

World world;
int clients[NUM_PLAYER];
struct sockaddr_in client_address[NUM_PLAYER];
int id;

int send_packet(const PacketHeader* packet, int sock){

	char buffer[BUFF_DIM];
	int ret;
	int size;

	size = Packet_serialize(buffer+sizeof(int), packet);
	memcpy(buffer, &size, 4);

	ret = send(sock, buffer, BUFF_DIM, 0);

	if(ret < 0){
		printf("error sending msg \n");
		return 0;
	}

	return 1;

}

int send_packet_udp(const PacketHeader* packet, int sock, struct sockaddr_in to){

	char buffer[UDP_SIZE];
	int ret;
	int size;

	memset(buffer, 0, UDP_SIZE);

	size = Packet_serialize(buffer+sizeof(int), packet);
	memcpy(buffer, &size, 4);

	int dim = sizeof(to);

	ret = sendto(sock, buffer, UDP_SIZE, 0, (struct sockaddr *)&to, dim );
	if(ret < 0){
		printf("error sending msg \n");
		return 0;
	}

	return 1;
}

PacketHeader* recive_packet(int sock){

	char buffer[BUFF_DIM];
	int ret;
	int size;

	ret = recv(sock, buffer, sizeof(buffer), MSG_WAITALL);
  	if(ret<=0){
		printf("error reciving msg \n");
		return 0;
	}

	memcpy(&size, buffer, 4);

	PacketHeader* packet = Packet_deserialize(buffer+sizeof(int), size);

	return packet;

}

PacketHeader* recive_packet_udp(int sock, struct sockaddr_in* from){

	char buffer[UDP_SIZE];
	int ret;
	int size;

	int dim = sizeof(from);

	ret = recvfrom(sock, buffer, UDP_SIZE, 0,  (struct sockaddr*) from, &dim );
	if (ret < 0){
		printf("error reciving msg\n");
		return 0;
	}

	memcpy(&size, buffer, 4);

	PacketHeader* packet = Packet_deserialize(buffer+sizeof(int), size);

	return packet;
}


PacketHeader* create_world_update_packet(World* w){

	PacketHeader h;
	h.type = WorldUpdate;

	ClientUpdate* c =(ClientUpdate*) malloc(sizeof(ClientUpdate)*(w->vehicles.size));

	ListItem* item=w->vehicles.first;
	int i=0;

  	while(item){
    	Vehicle* v=(Vehicle*)item;
    	c[i].id=v->id;
    	c[i].x =v->x;
    	c[i].y =v->y;
    	c[i].theta =v->theta;
    	item=item->next;
    	i++;
 	 }

 	 WorldUpdatePacket* p = (WorldUpdatePacket*)malloc(sizeof(WorldUpdatePacket));
 	 p->header = h;
 	 p->num_vehicles = w->vehicles.size;
 	 p->updates = c;

 	 return (PacketHeader*)p;
}

void* udp_start(void* arguments){

	printf("[UDP] Started\n");

	sockets* socks = (sockets*) arguments;

 	while(1){

 		struct sockaddr_in client;
 		
		PacketHeader* packet = recive_packet_udp(socks->udp_sock, &client);

		switch (packet->type){

			case 0x1: {

				IdPacket* idp = (IdPacket*)packet;
				printf("PacketHeader type: %d from client id %d\n", packet->type, idp->id);
				break;

			}

			case 0x2: {



				IdPacket* v_id = (IdPacket*)packet;
				Vehicle* v = World_getVehicle(&world, v_id->id);

				IdPacket* client_id =(IdPacket*)recive_packet(clients[v_id->id-1]);

				printf("Richiesta texture del veicolo %d dal client %d\n", v_id->id, client_id->id );

				ImagePacket* v_text = (ImagePacket*)malloc(sizeof(ImagePacket));
	  			PacketHeader v_head;
	  			v_head.type = PostTexture;
	  			v_text->header = v_head;
	  			v_text->image = v->texture;
	  			v_text->id = v_id->id;


	  			printf("Trovato!\nInvio texture...");
	  			send_packet((PacketHeader*)v_text, clients[v_id->id-1]);
	  			printf("Completato\n");
	  			break;


			}
			
			case 0x7: {

				VehicleUpdatePacket* vp = (VehicleUpdatePacket*)packet;

				client_address[vp->id-1] = client;

				//printf("<id %d, rotational_force %f, translation_force %f>\n",vp->id, vp->rotational_force, vp->translational_force);
				Vehicle* v = World_getVehicle(&world, vp->id);
				v->translational_force_update = vp->translational_force;
				v->rotational_force_update = vp->rotational_force;
				World_update(&world);
				WorldUpdatePacket* ups = (WorldUpdatePacket*) create_world_update_packet(&world);

				send_packet_udp((PacketHeader*)ups,socks->udp_sock, client);
				break;

			}

			default: {

				printf("Unknown packet\n");
				break;
				
			}
		}
	}

}


int main(int argc, char **argv){

	if (argc<3) {
    	printf("usage: %s <elevation_image> <texture_image>\n", argv[1]);
    	exit(-1);
  	}

  	char* elevation_filename=argv[1];
  	char* texture_filename=argv[2];

  	pthread_t client_thread;

  	printf("[SERVER] Server starting...\n");

  	printf("loading elevation image from %s ... ", elevation_filename);


  	Image* surface_elevation = Image_load(elevation_filename);
  	if (surface_elevation) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}


  	printf("loading texture image from %s ... ", texture_filename);
  	Image* surface_texture = Image_load(texture_filename);
  	if (surface_texture) {
    	printf("Done! \n");
  	} else {
    	printf("Fail! \n");
  	}

  	id = 1;

	int tcp = socket(AF_INET, SOCK_STREAM, 0);
	if(tcp<0){
		printf("error creating socket\n");
		return 0;
	}

	struct sockaddr_in tcp_addr;
	tcp_addr.sin_family = AF_INET;
	tcp_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	tcp_addr.sin_port = htons(27015);

	int ret;
	ret = bind(tcp, (struct sockaddr*) &tcp_addr, sizeof(tcp_addr));
	if (ret<0){
		printf("error binding socket\n");
		return 0;
	}

	ret = listen(tcp,10);
	if (ret<0){
		printf("error listen socket\n");
		return 0;
	}

	int udp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if(udp<0){
		printf("error creating socket\n");
		return 0;
	}

	struct sockaddr_in udp_addr; 
	udp_addr.sin_family = AF_INET;
	udp_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	udp_addr.sin_port = htons(27014);

	ret = bind(udp, (struct sockaddr*) &udp_addr, sizeof(udp_addr));
	if (ret<0){
		printf("binding error\n");
		return 0;
	}

	sockets info;
	info.tcp_sock = tcp;
	info.udp_sock = udp;
	info.id = id;

	pthread_create(&client_thread, NULL, udp_start, (void*)&info);
	pthread_detach(client_thread);

	World_init(&world, surface_elevation, surface_texture, 0.5, 0.5, 0.5);

	printf("[SERVER] Started\n");

	int socket_client;
	struct sockaddr_in client;
	int dim = sizeof(client);

	while(1){

		socket_client = accept(tcp, (struct sockaddr*) &client, &dim);
		if (socket_client < 0){
			printf("connection error\n");
			return 0;
		}

		clients[id-1] = socket_client;
		printf("Ho registrato in clients[%d] il socket: %d", id-1, clients[id-1]);

		printf("[SERVER] Client connected to the server\n");

		printf("[SERVER] Assigning client id and receive vehicle texture\n");

  		IdPacket* id_packet = (IdPacket*)recive_packet(socket_client);
  		id_packet->id = id;

  		send_packet((PacketHeader*)id_packet, socket_client);

  		ImagePacket* image_from_client = (ImagePacket*)recive_packet(socket_client);

  		Image_save(image_from_client->image, "imagefromclient.ppm");

  		printf("[SERVER] Create vehicle for client %d\n", id);

  		Vehicle* vehicle=(Vehicle*)malloc(sizeof(Vehicle));
  		Vehicle_init(vehicle, &world, id, image_from_client->image);
  		World_addVehicle(&world, vehicle);

  		printf("[SERVER] Sending server texture to the client...\n");

	  	ImagePacket* elevation = (ImagePacket*)malloc(sizeof(ImagePacket));
	  	PacketHeader e_head;
	  	e_head.type = PostElevation;
	  	elevation->header = e_head;
	  	elevation->image = surface_elevation;
	  	elevation->id = 0;
	  	send_packet((PacketHeader*)elevation, socket_client);

	  	ImagePacket* texture = (ImagePacket*)malloc(sizeof(ImagePacket));
	  	PacketHeader t_head;
	  	t_head.type = PostTexture;
	  	texture->header = t_head;
	  	texture->image = surface_texture;
	  	texture->id = 0;
	  	send_packet((PacketHeader*)texture, socket_client);

      	id++;
	}

	return 0;

}